package org.orienlabs.orienworkx.utils;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

/**
 * @author kumar.anil
 *
 */

public class AppiumService {
	static ThreadLocal<AppiumDriverLocalService> runningService = new ThreadLocal<>();
	static String start() {
		AppiumServiceBuilder b = new AppiumServiceBuilder();
		b.usingAnyFreePort()
		 .withIPAddress("127.0.0.1")
		 .withArgument(GeneralServerFlag.LOG_TIMESTAMP);
		
		b.build();
		
		runningService.set(AppiumDriverLocalService.buildService(b));
		runningService.get().start();
		return runningService.get().getUrl().toString();
	}

	public static synchronized String getUrl() {
		if (runningService.get() == null)
			start();
		return runningService.get().getUrl().toString();		
	}
}