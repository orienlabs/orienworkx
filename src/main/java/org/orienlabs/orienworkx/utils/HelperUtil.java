package org.orienlabs.orienworkx.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.util.Hash;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.orienlabs.orienworkx.config.Config;

import lombok.extern.java.Log;

@Log
public class HelperUtil {
	static Map<String, String> currencySymbols= new HashMap<>();
	static {
		currencySymbols.put("AED", "&#1583;.&#1573;"); // ?
		currencySymbols.put("AFN", "&#65;&#102;");
		currencySymbols.put("ALL", "&#76;&#101;&#107;");
		currencySymbols.put("AMD", "");
		currencySymbols.put("ANG", "&#402;");
		currencySymbols.put("AOA", "&#75;&#122;"); // ?
		currencySymbols.put("ARS", "&#36;");
		currencySymbols.put("AUD", "&#36;");
		currencySymbols.put("AWG", "&#402;");
		currencySymbols.put("AZN", "&#1084;&#1072;&#1085;");
		currencySymbols.put("BAM", "&#75;&#77;");
		currencySymbols.put("BBD", "&#36;");
		currencySymbols.put("BDT", "&#2547;"); // ?
		currencySymbols.put("BGN", "&#1083;&#1074;");
		currencySymbols.put("BHD", ".&#1583;.&#1576;"); // ?
		currencySymbols.put("BIF", "&#70;&#66;&#117;"); // ?
		currencySymbols.put("BMD", "&#36;");
		currencySymbols.put("BND", "&#36;");
		currencySymbols.put("BOB", "&#36;&#98;");
		currencySymbols.put("BRL", "&#82;&#36;");
		currencySymbols.put("BSD", "&#36;");
		currencySymbols.put("BTN", "&#78;&#117;&#46;"); // ?
		currencySymbols.put("BWP", "&#80;");
		currencySymbols.put("BYR", "&#112;&#46;");
		currencySymbols.put("BZD", "&#66;&#90;&#36;");
		currencySymbols.put("CAD", "&#36;");
		currencySymbols.put("CDF", "&#70;&#67;");
		currencySymbols.put("CHF", "&#67;&#72;&#70;");
		currencySymbols.put("CLF", ""); // ?
		currencySymbols.put("CLP", "&#36;");
		currencySymbols.put("CNY", "&#165;");
		currencySymbols.put("COP", "&#36;");
		currencySymbols.put("CRC", "&#8353;");
		currencySymbols.put("CUP", "&#8396;");
		currencySymbols.put("CVE", "&#36;"); // ?
		currencySymbols.put("CZK", "&#75;&#269;");
		currencySymbols.put("DJF", "&#70;&#100;&#106;"); // ?
		currencySymbols.put("DKK", "&#107;&#114;");
		currencySymbols.put("DOP", "&#82;&#68;&#36;");
		currencySymbols.put("DZD", "&#1583;&#1580;"); // ?
		currencySymbols.put("EGP", "&#163;");
		currencySymbols.put("ETB", "&#66;&#114;");
		currencySymbols.put("EUR", "&#8364;");
		currencySymbols.put("FJD", "&#36;");
		currencySymbols.put("FKP", "&#163;");
		currencySymbols.put("GBP", "&#163;");
		currencySymbols.put("GEL", "&#4314;"); // ?
		currencySymbols.put("GHS", "&#162;");
		currencySymbols.put("GIP", "&#163;");
		currencySymbols.put("GMD", "&#68;"); // ?
		currencySymbols.put("GNF", "&#70;&#71;"); // ?
		currencySymbols.put("GTQ", "&#81;");
		currencySymbols.put("GYD", "&#36;");
		currencySymbols.put("HKD", "&#36;");
		currencySymbols.put("HNL", "&#76;");
		currencySymbols.put("HRK", "&#107;&#110;");
		currencySymbols.put("HTG", "&#71;"); // ?
		currencySymbols.put("HUF", "&#70;&#116;");
		currencySymbols.put("IDR", "&#82;&#112;");
		currencySymbols.put("ILS", "&#8362;");
		currencySymbols.put("INR", "&#8377;");
		currencySymbols.put("IQD", "&#1593;.&#1583;"); // ?
		currencySymbols.put("IRR", "&#65020;");
		currencySymbols.put("ISK", "&#107;&#114;");
		currencySymbols.put("JEP", "&#163;");
		currencySymbols.put("JMD", "&#74;&#36;");
		currencySymbols.put("JOD", "&#74;&#68;"); // ?
		currencySymbols.put("JPY", "&#165;");
		currencySymbols.put("KES", "&#75;&#83;&#104;"); // ?
		currencySymbols.put("KGS", "&#1083;&#1074;");
		currencySymbols.put("KHR", "&#6107;");
		currencySymbols.put("KMF", "&#67;&#70;"); // ?
		currencySymbols.put("KPW", "&#8361;");
		currencySymbols.put("KRW", "&#8361;");
		currencySymbols.put("KWD", "&#1583;.&#1603;"); // ?
		currencySymbols.put("KYD", "&#36;");
		currencySymbols.put("KZT", "&#1083;&#1074;");
		currencySymbols.put("LAK", "&#8365;");
		currencySymbols.put("LBP", "&#163;");
		currencySymbols.put("LKR", "&#8360;");
		currencySymbols.put("LRD", "&#36;");
		currencySymbols.put("LSL", "&#76;"); // ?
		currencySymbols.put("LTL", "&#76;&#116;");
		currencySymbols.put("LVL", "&#76;&#115;");
		currencySymbols.put("LYD", "&#1604;.&#1583;"); // ?
		currencySymbols.put("MAD", "&#1583;.&#1605;."); // ?
		currencySymbols.put("MDL", "&#76;");
		currencySymbols.put("MGA", "&#65;&#114;"); // ?
		currencySymbols.put("MKD", "&#1076;&#1077;&#1085;");
		currencySymbols.put("MMK", "&#75;");
		currencySymbols.put("MNT", "&#8366;");
		currencySymbols.put("MOP", "&#77;&#79;&#80;&#36;"); // ?
		currencySymbols.put("MRO", "&#85;&#77;"); // ?
		currencySymbols.put("MUR", "&#8360;"); // ?
		currencySymbols.put("MVR", ".&#1923;"); // ?
		currencySymbols.put("MWK", "&#77;&#75;");
		currencySymbols.put("MXN", "&#36;");
		currencySymbols.put("MYR", "&#82;&#77;");
		currencySymbols.put("MZN", "&#77;&#84;");
		currencySymbols.put("NAD", "&#36;");
		currencySymbols.put("NGN", "&#8358;");
		currencySymbols.put("NIO", "&#67;&#36;");
		currencySymbols.put("NOK", "&#107;&#114;");
		currencySymbols.put("NPR", "&#8360;");
		currencySymbols.put("NZD", "&#36;");
		currencySymbols.put("OMR", "&#65020;");
		currencySymbols.put("PAB", "&#66;&#47;&#46;");
		currencySymbols.put("PEN", "&#83;&#47;&#46;");
		currencySymbols.put("PGK", "&#75;"); // ?
		currencySymbols.put("PHP", "&#8369;");
		currencySymbols.put("PKR", "&#8360;");
		currencySymbols.put("PLN", "&#122;&#322;");
		currencySymbols.put("PYG", "&#71;&#115;");
		currencySymbols.put("QAR", "&#65020;");
		currencySymbols.put("RON", "&#108;&#101;&#105;");
		currencySymbols.put("RSD", "&#1044;&#1080;&#1085;&#46;");
		currencySymbols.put("RUB", "&#8381;");
		currencySymbols.put("RWF", "&#1585;.&#1587;");
		currencySymbols.put("SAR", "&#65020;");
		currencySymbols.put("SBD", "&#36;");
		currencySymbols.put("SCR", "&#8360;");
		currencySymbols.put("SDG", "&#163;"); // ?
		currencySymbols.put("SEK", "&#107;&#114;");
		currencySymbols.put("SGD", "&#36;");
		currencySymbols.put("SHP", "&#163;");
		currencySymbols.put("SLL", "&#76;&#101;"); // ?
		currencySymbols.put("SOS", "&#83;");
		currencySymbols.put("SRD", "&#36;");
		currencySymbols.put("STD", "&#68;&#98;"); // ?
		currencySymbols.put("SVC", "&#36;");
		currencySymbols.put("SYP", "&#163;");
		currencySymbols.put("SZL", "&#76;"); // ?
		currencySymbols.put("THB", "&#3647;");
		currencySymbols.put("TJS", "&#84;&#74;&#83;"); // ? TJS (guess)
		currencySymbols.put("TMT", "&#109;");
		currencySymbols.put("TND", "&#1583;.&#1578;");
		currencySymbols.put("TOP", "&#84;&#36;");
		currencySymbols.put("TRY", "&#8356;"); // New Turkey Lira (old symbol// used)
		currencySymbols.put("TTD", "&#36;");
		currencySymbols.put("TWD", "&#78;&#84;&#36;");
		currencySymbols.put("TZS", "");
		currencySymbols.put("UAH", "&#8372;");
		currencySymbols.put("UGX", "&#85;&#83;&#104;");
		currencySymbols.put("USD", "&#36;");
		currencySymbols.put("UYU", "&#36;&#85;");
		currencySymbols.put("UZS", "&#1083;&#1074;");
		currencySymbols.put("VEF", "&#66;&#115;");
		currencySymbols.put("VND", "&#8363;");
		currencySymbols.put("VUV", "&#86;&#84;");
		currencySymbols.put("WST", "&#87;&#83;&#36;");
		currencySymbols.put("XAF", "&#70;&#67;&#70;&#65;");
		currencySymbols.put("XCD", "&#36;");
		currencySymbols.put("XDR", "");
		currencySymbols.put("XOF", "");
		currencySymbols.put("XPF", "&#70;");
		currencySymbols.put("YER", "&#65020;");
		currencySymbols.put("ZAR", "&#82;");
		currencySymbols.put("ZMK", "&#90;&#75;"); // ?
		currencySymbols.put("ZWL", "&#90;&#36;");
	}
	
	public static void copyResource(String sourceFile, String destinationFile) throws IOException
	{
		FileUtils.copyURLToFile(
				HelperUtil.class.getClassLoader().getResource(sourceFile), new File(destinationFile));
	}
	
	/**
	 * Get relative path of icon for using in html reports
	 * @param forFile {@link: File} for which icon is required
	 * @param relativeFolder {@link: String} folder containing file where icon is to be embedded
	 * @return
	 */
	public static String getIconForFile(File forFile, File relativeFolder)
	{
		String extn= FilenameUtils.getExtension(forFile.getName()).toLowerCase();
		File iconPath= Config.getIconFolder();
		File iconFile= new File(iconPath, "icon-txt.png");
		switch (extn) {
		case "html":
			iconFile= new File(iconPath, "icon-html.png");
			break;
		case "pdf":
			iconFile= new File(iconPath, "icon-pdf.png");
			break;
		case "png":
			//iconFile= new File(iconPath, "icon-png.png");
			iconFile= forFile;
			break;
		case "ppt":
		case "pptx":
			iconFile= new File(iconPath, "icon-ppt.png");
			break;
		case "txt":
			iconFile= new File(iconPath, "icon-txt.png");
			break;
		case "xls":
		case "xlsx":
			iconFile= new File(iconPath, "icon-xls.png");
			break;
		case "xml":
			iconFile= new File(iconPath, "icon-xml.png");
			break;
		case "zip":
			iconFile= new File(iconPath, "icon-zip.png");
			break;
		default:
			break;
		}
		
		return 
				StringUtils.strip(
						iconFile.getAbsolutePath().replace(relativeFolder.getAbsolutePath(), ""),
						"/\\");
	}
	
	/**
	 * Copy screenshot to local persistant folder and returns a relative path for attaching in html reports
	 * @param scrnShot
	 * @param relativeFolder
	 * @return
	 * @throws IOException 
	 */
	public static String getScreenshotPath(File scrnShot, File relativeFolder) throws IOException
	{
		return 
				StringUtils.strip(
						copyScreenshotToLocal(scrnShot).getAbsolutePath().replace(relativeFolder.getAbsolutePath(), ""),
						"/\\");
	}
	
	/**
	 * Copy screenshot to local directory
	 * @param scrnshot
	 * @return
	 * @throws IOException 
	 */
	public static File copyScreenshotToLocal(File scrnshot) throws IOException
	{
		File scrnshotFolder= Config.getScreenshotFolder();
		log.info("Copying screenshot to " + scrnshotFolder.getAbsolutePath());
		if(scrnshot.getAbsolutePath().startsWith(scrnshotFolder.getAbsolutePath()))
			return scrnshot;
		
		File copiedFile= new File(scrnshotFolder, scrnshot.getName());
		FileUtils.copyFile(scrnshot, copiedFile);
		log.info("Copied screenshot to " + scrnshotFolder.getAbsolutePath());
		return copiedFile;
	}

	public static void sleepForSeconds(int sec) {
		sleep(sec * 1000);
	}
	
	public static void sleep(int millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {}
	}
	
	public static float toFloat(String instr)
	{
		instr= stripNonNumeralsAndDot(instr);
		if(instr.isEmpty())
			instr= "0";
		
		return Float.parseFloat(stripNonNumeralsAndDot(instr));
	}
	
	public static String stripNonNumeralsAndDot(String instr)
	{
		return instr.replaceAll("[^\\d.]", "");
	}
	
	public static String stripEmptyLines(String text)
	{
		LinkedList<String> lines= new LinkedList<String>();
		StringBuffer sb = new StringBuffer();
		for (String str : text.split("[\\r\\n]+")) {
			if(!str.trim().isEmpty()) lines.add(str);
		}
		return StringUtils.join(lines, "\n");
	}
	
	public static String keepCharacters(String instr, List<Character> charactersToKeep)
	{
		StringBuilder sb= new StringBuilder();
		for (char c : instr.toCharArray()) {
			if(charactersToKeep.contains(c))
				sb.append(c);
		}
		
		return sb.toString();
	}
	
	public static File downloadFile(String url, String extn) throws Throwable
	{
		File f= File.createTempFile("dwld", "." + extn);
		FileUtils.copyURLToFile(new URL(url), f);
		return f;
	}
	
	public static String getCurrencySymbol(String currencyAbbreviation)
	{
		if(StringUtils.isBlank(currencyAbbreviation))
			return currencyAbbreviation;
		
		return StringEscapeUtils.unescapeHtml4(currencySymbols.get(currencyAbbreviation));
	}
	
	public static int getRandomNumber(int min, int max)
	{
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	public static String removeAuthFromUrl(String url)
	{
		int end= url.indexOf("@") + 1;
		if  (end <= 0) return url;
		int start= url.indexOf("//") + 2;
		String toReplace= StringUtils.mid(url, start, end- start);
		return StringUtils.replace(url, toReplace, "", 1);
	}
	
	public static String addAuthToUrl(String url, String username, String password)
	{
		throw new NotImplementedException();
	}
}
