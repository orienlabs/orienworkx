package org.orienlabs.orienworkx.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Data;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Instance;

public class GoogleAPI {
	Compute computeService = null;
	File credentialJsonFile= null;
	String projectName= null;
	
	public GoogleAPI(File credentialJsonFile, String project)
	{
		this.credentialJsonFile= credentialJsonFile;
		this.projectName= project;
	}
	
	public GoogleAPI connect() throws Exception
	{
		computeService = createComputeService();
		return this;
	}
	
	private GoogleCredential authorize() throws IOException {
    	GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(credentialJsonFile));
    	return credential;
    }
    
    private Compute createComputeService() throws IOException, GeneralSecurityException {
	    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();

	    GoogleCredential credential = authorize();
	    if (credential.createScopedRequired()) {
	      credential =
	          credential.createScoped(Arrays.asList("https://www.googleapis.com/auth/cloud-platform"));
	    }

	    return new Compute.Builder(httpTransport, jsonFactory, credential)
	        .setApplicationName("Google-ComputeSample/0.1")
	        .build();
	  }
    
    public String getPublicIP(String zone, String instanceName) throws Exception
	{
		return getInstance(zone, instanceName).getNetworkInterfaces().get(0).getAccessConfigs().get(0).getNatIP();
	}
	
	private Instance getInstance(String zone, String instanceName) throws Exception
	{
		return computeService.instances().get(projectName, zone, instanceName).execute();
	}
	
	public Instance startInstance(String zone, String instanceName) throws Exception
	{
		computeService.instances().start(projectName, zone, instanceName).execute();
		waitForStatus(projectName, zone, instanceName, "RUNNING");
		return getInstance(zone, instanceName);
	}
	
	public String getStatus(String zone, String instanceName) throws Exception
	{
		return getInstance(zone, instanceName).getStatus();
	}
	
	public Instance stopInstance(String zone, String instanceName) throws Exception
	{
		computeService.instances().stop(projectName, zone, instanceName).execute();
		waitForStatus(projectName, zone, instanceName, "TERMINATED");
		return getInstance(zone, instanceName);
	}
	
	private void waitForStatus(String projectName, String zone, String instanceName, String status) throws Exception
	{
		//Wait for instance to stop
		for (int i = 0; i < 60; i++) {
			Instance instance= getInstance(zone, instanceName);
			if(instance.getStatus().equalsIgnoreCase(status))
				break;
			
			Thread.sleep(1000);
		}
	}
}
