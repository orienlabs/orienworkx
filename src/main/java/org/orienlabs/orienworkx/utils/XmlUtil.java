package org.orienlabs.orienworkx.utils;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlUtil {

	/**
	 * Update value of all xml nodes matching with given xpath
	 * @param xmlFile
	 * @param xPath
	 * @param nodeValue new value of node
	 * @throws Exception 
	 */
	public void updateNodeValue(File xmlFile, String xPath, String nodeValue) throws Exception
	{
		NodeList nodes = findXmlElementsByXPath(xPath, xmlFile.getAbsolutePath());

		if (nodes.getLength() == 0) { throw new Exception("No nodes matching to xpath:" + xPath + " could be found..."); }

		boolean replaced = false;
		for (int i = 0; i < nodes.getLength(); i++)
		{
			// Replace only if there is a direct and only text node inside the element
			// When there is only one child (which is text node), replace
			if (nodes.item(i).getChildNodes().getLength() <= 1)
			{
				replaced = true;
				nodes.item(i).setTextContent(nodeValue);
			}
		}

		if (replaced) writeXmltoFile(xmlFile, nodes.item(0).getOwnerDocument());
	}
	
	public void updateAttributeValue(File xmlFile, String xpathForNodes, String attrName, String attrValue) throws Exception
	{
		NodeList nodes = findXmlElementsByXPath(xpathForNodes, xmlFile.getAbsolutePath());
		for (int i = 0; i < nodes.getLength(); i++)
		{
			try
			{
				nodes.item(i).getAttributes().getNamedItem(attrName).setNodeValue(attrValue);
			} catch (NullPointerException e)
			{
				NamedNodeMap atts = nodes.item(i).getAttributes();
				Attr att = nodes.item(i).getOwnerDocument().createAttribute(attrName);
				att.setValue(attrValue);
				atts.setNamedItem(att);
			}
		}

		if (nodes.getLength() > 0) writeXmltoFile(xmlFile, nodes.item(0).getOwnerDocument());
	}
	
	public File writeXmltoFile(File outFile, Document xmlDoc) throws Exception
	{
		return writeXmltoFile(outFile, xmlDoc, true);
	}
	
	public File writeXmltoFile(File outFile, Document xmlDoc, boolean indent) throws Exception
	{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();

		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");

		if (indent)
		{
			transformer.setOutputProperty("{http://xml.apache.org/xalan}indent-amount", "2");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		}

		DOMSource source = new DOMSource(xmlDoc);
		// StreamResult result = new StreamResult(outFile);

		StringWriter writer = new StringWriter();
		transformer.transform(source, new StreamResult(writer));
		// String output = writer.toString();

		FileUtils.writeStringToFile(outFile, writer.toString(), "UTF-8");
		
		writer.flush();
		writer.close();
		return outFile;
	}
	
	public NodeList findXmlElementsByXPath(Document doc, String xpathString) throws Exception
	{
		NodeList nodes = null;
		try
		{
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();

			XPathExpression expr = xpath.compile(xpathString);
			Object result = expr.evaluate(doc, XPathConstants.NODESET);
			nodes = (NodeList) result;
		} catch (Exception e)
		{
			throw e;
		} finally
		{
		}

		return nodes;
	}
	
	public NodeList findXmlElementsByXPath(String xpathString, String filePathOrXmlString) throws Exception
	{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setNamespaceAware(true);

		domFactory.setValidating(false);
		domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

		DocumentBuilder builder;
		Document doc;

		builder = domFactory.newDocumentBuilder();

		if(filePathOrXmlString.contains("<"))
			doc =  builder.parse(new InputSource(new StringReader(filePathOrXmlString)));
		else
			doc= builder.parse(new File(filePathOrXmlString));
		
		return findXmlElementsByXPath(doc, xpathString);
	}
}