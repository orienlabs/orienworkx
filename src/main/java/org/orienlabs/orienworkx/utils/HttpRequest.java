package org.orienlabs.orienworkx.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


/**
 * @author kumar.anil
 *
 */

@SuppressWarnings("deprecation")
public class HttpRequest{

    HttpClient httpClient;
    HttpContext localContext;
    private String ret;

    HttpResponse response = null;
    HttpPost httpPost = null;
    HttpGet httpGet = null;
    
    URIBuilder uriBuilder = new URIBuilder();
    
    public HttpRequest withHeader(String name, String value)
    {
    	httpPost.addHeader(name, value);
    	return this;
    }
    
    public HttpRequest withHeader(Header header)
    {
    	httpPost.addHeader(header);
    	return this;
    }
    
    public HttpRequest withUrlParameter(String paramName, String paramValue)
    {
    	uriBuilder.addParameter(paramName, paramValue);
    	return this;
    }
    
    public HttpRequest withBody(String body)
    {
    	httpPost.setEntity(new StringEntity(body, "UTF-8"));return this;
    }
    
    public HttpRequest withBody(JSONObject body)
    {
    	return withHeader("Content-Type", "application/json").withBody(body.toString());
    }
    
    public String get()
    {
    	return null;
    }
    
    public HttpResponse post() throws ClientProtocolException, IOException, URISyntaxException
    {
    	httpPost.setURI(uriBuilder.build());
    	return httpClient.execute(httpPost,localContext);
    }
    
    public HttpRequest(){
        httpClient = HttpClients.createDefault(); 
        localContext = new BasicHttpContext();    
    }
    
    public HttpRequest(String baseUrl) throws URISyntaxException{
        this();
        uriBuilder= new URIBuilder(baseUrl);
        httpPost = new HttpPost();
        httpGet= new HttpGet();
    }

    public void clearCookies() {
        //httpClient.getCookieStore().clear();
    }

    public void abort() {
        try {
            if (httpClient != null) {
                System.out.println("Abort.");
                httpPost.abort();
            }
        } catch (Exception e) {
            System.out.println("Your App Name Here" + e);
        }
    }

    public String sendPost(String url, String data) {
        return sendPost(url, data, null);
    }

    public String sendJSONPost(String url, JSONObject data) {
        return sendPost(url, data.toString(), "application/json");
    }

    public String sendPost(String url, String data, String contentType) {
        ret = null;

        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);

        httpPost = new HttpPost(url);
        response = null;

        StringEntity tmp = null;        

        httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36");
        httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");

        if (contentType != null) {
            httpPost.setHeader("Content-Type", contentType);
        } else {
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        tmp = new StringEntity(data,"UTF-8");
        httpPost.setEntity(tmp);

        try {
            response = httpClient.execute(httpPost,localContext);

            if (response != null) {
                ret = EntityUtils.toString(response.getEntity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public String sendGet(String url) throws ParseException, IOException {
        httpGet = new HttpGet(url);  

        try {
            response = httpClient.execute(httpGet);  
        } catch (Exception e) {
            e.printStackTrace();
        }

        ret = EntityUtils.toString(response.getEntity());  
        return ret;
    }

    public InputStream getHttpStream(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString); 
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))                     
            throw new IOException("Not an HTTP connection");

        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect(); 

            response = httpConn.getResponseCode();                 

            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();                                 
            }                     
        } catch (Exception e) {
            throw new IOException("Error connecting");            
        } // end try-catch

        return in;     
    }
}