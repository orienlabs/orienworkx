package org.orienlabs.orienworkx.cucumber;

import org.orienlabs.orienworkx.robot.UIDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;

public class CucumberHooks {
	
	@After
	public void embedScreenshot(Scenario scenario) {

		try {
			UIDriver.takePngScreenshot(true);
		} catch (Throwable e) {
		}
	}
}