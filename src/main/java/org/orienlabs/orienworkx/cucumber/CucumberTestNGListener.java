package org.orienlabs.orienworkx.cucumber;

import org.orienlabs.orienworkx.logger.AbstractListener;
import org.orienlabs.orienworkx.logger.AbstractListener.ExecutionMethod;
import org.testng.annotations.AfterMethod;

public class CucumberTestNGListener extends AbstractListener {
	
	static{
		System.setProperty("org.freemarker.loggerLibrary", "none");
		setExecutionMethod(ExecutionMethod.BDD);
	}
}
