package org.orienlabs.orienworkx.cucumber;

import java.io.File;

import org.apache.commons.lang3.RandomStringUtils;
import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.robot.UIDriver;
import org.orienlabs.orienworkx.robot.UIElement;
import cucumber.api.java.en.Given;

public class StepDefinition {
	
	/**
	 * I have launched browser
	 * @throws Throwable
	 */
	@Given("^I have launched browser$")
	public void launchBrowser() throws Throwable {
		UIDriver.startBrowser();
	}
	
	/**
	 * I navigate to 'http://www.google.com'
	 * @param url
	 * @throws Throwable
	 */
	@Given("^I navigate to '(.*)'$")
	public void navigateTo(String url) throws Throwable {
		UIDriver.navigate(
				Config.resolve(url));
	}
	
	/**
	 * I type 'test anything' in textbox > name=q
	 * @param text
	 * @param elementType
	 * @param locator
	 * @throws Throwable
	 */
	@Given("^I type '(.*)' in (.*) > (.*)$")
	public void typeText(String text, String elementType, String locator) throws Throwable {
		new UIElement(locator).setText(
				Config.resolve(text));
	}
	
	/**
	 * I submit form > name=q
	 * @param locator
	 * @throws Throwable
	 */
	@Given("^I submit form > (.*)$")
	public void typeText(String locator) throws Throwable {
		new UIElement(locator).submit();
	}
	
	@Given("^parameter \"(.*)\" is set as \"(.*)\"$")
	public void setParameter(String paramName, String paramValue) throws Throwable {
		Config.set(paramName, Config.resolve(paramValue));
	}
	
	@Given("^parameter \"(.*)\" is set as$")
	public void setMultilineParameter(String paramName, String multilineString) throws Throwable {
		Config.set(paramName, Config.resolve(multilineString));
	}
	
	@Given("^a random alphanumeric string of \"(.*)\" characters is stored as \"(.*)\"$")
	public void randomAlpha(int length, String paramName) throws Throwable {
		Config.set(paramName, RandomStringUtils.randomAlphanumeric(length));
	}
	
	@Given("^parameter \"(.*)\" is set as random alpha string of \"(.*)\" characters$")
	public void setParameter(String paramName, int paramSize) throws Throwable {
		Config.set(paramName, RandomStringUtils.randomAlphabetic(paramSize, paramSize+1));
	}
	
	@Given("^parameters are loaded from \"(.*)\"$")
	public void loadParameters(String paramFile) throws Throwable {
		Config.setAll(new File(paramFile));
	}
}