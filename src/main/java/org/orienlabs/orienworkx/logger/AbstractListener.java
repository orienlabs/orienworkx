package org.orienlabs.orienworkx.logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.robot.UIDriver;
import org.orienlabs.orienworkx.utils.HelperUtil;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class AbstractListener implements ITestListener, ISuiteListener{
	public enum ExecutionMethod { KEYWORD, BDD, POM }
	static ExecutionMethod listenerType;

	static {
		try {
			String[] icons= new String[]{
					"icon-bug.png","icon-html.png","icon-pdf.png",
					"icon-png.png","icon-ppt.png","icon-txt.png",
					"icon-xls.png","icon-xml.png","icon-zip.png"};
			for (String icon : icons)
				HelperUtil.copyResource("icons/" + icon, new File(Config.getIconFolder(), icon).getPath());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void setExecutionMethod(ExecutionMethod executionMethod)
	{
		listenerType= executionMethod;
	}
	
	public static ExecutionMethod getExecutionMethod()
	{
		return listenerType;
	}
	
	public static File getScreenshotFolder()
	{
		return new File(Config.getReportFolder(), "scrnshots");
	}

	@Override
	public synchronized void onStart(ITestContext context) {
	}

	@Override
	public synchronized void onFinish(ITestContext context) {
		quitTestCase();
	}
	
	@Override
	public synchronized void onTestStart(ITestResult result) {
		Map<String, String> params= result.getTestContext().getCurrentXmlTest().getAllParameters();
		Config.setAll(params);
	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {
		quitTestCase();
	}

	@Override
	public synchronized void onTestFailure(ITestResult result) {
		quitTestCase();
	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {
		quitTestCase();
	}

	@Override
	public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		quitTestCase();
	}
    
    @Override
	public void onStart(ISuite suite) {
		try {
			Config.class.newInstance();
			String dataProviderThreadCount= Config.get("dataproviderthreadcount", "");
			if(!dataProviderThreadCount.isEmpty())
				suite.getXmlSuite().setDataProviderThreadCount(Integer.parseInt(dataProviderThreadCount));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    
    public static void quitTestCase()
    {
    	try {
    		if(UIDriver.getDriver() != null) UIDriver.takePngScreenshot(true);
		} catch (Throwable e) {}
    	
    	if(Config.get("BROWSER_CLOSE_STRATEGY", "AUTO").equalsIgnoreCase("AUTO"))
    		UIDriver.quit();
    }
    
	@Override
	public void onFinish(ISuite suite) {		
	}
}
