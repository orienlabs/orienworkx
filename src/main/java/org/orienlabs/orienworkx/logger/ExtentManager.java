package org.orienlabs.orienworkx.logger;

import java.io.File;

import org.orienlabs.orienworkx.config.Config;
import org.springframework.stereotype.Component;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

@Component
public class ExtentManager {
	static ExtentReports extent;
    
	public static synchronized ExtentReports getInstance() {
    	if (extent == null)
    		createInstance(new File(Config.getReportFolder() , "extent.html"));
    	
        return extent;
    }
    
    static synchronized ExtentReports createInstance(File file) {
    	file.getParentFile().mkdirs();
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(file);
        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config().setChartVisibilityOnOpen(false);
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setDocumentTitle(file.getName());
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(file.getName());
        
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.setAnalysisStrategy(AnalysisStrategy.TEST);
        flush();
        
        return extent;
    }
    
    public static synchronized void flush()
    {
    	extent.flush();
    }
}