package org.orienlabs.orienworkx.logger;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.keyword.KeywordTestNGListener;
import org.orienlabs.orienworkx.logger.AbstractListener.ExecutionMethod;

import com.aventstack.extentreports.Status;
import com.cucumber.listener.Reporter;

public class Logger {
	public static void logStep(String description, Status status, Object stepOutcome, HashMap<File, String> stepScreenshots) throws Throwable
	{
		KeywordTestNGListener.logStep(description, status, stepOutcome, stepScreenshots);
	}
	
	public static void logStep(String details) throws Throwable
	{
		if(AbstractListener.listenerType.equals(ExecutionMethod.BDD))
			Reporter.addStepLog(details);
	}

	public static void attachScreenshot(File scrnshot) throws IOException {
		String relativePath= scrnshot.getAbsolutePath().replace(Config.getReportFolder().getAbsolutePath(), "");
		if(!relativePath.equalsIgnoreCase(scrnshot.getAbsolutePath()))
			relativePath = "." + relativePath;
		
		Reporter.addScreenCaptureFromPath(relativePath);
	}
}