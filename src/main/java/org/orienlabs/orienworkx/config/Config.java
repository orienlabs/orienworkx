package org.orienlabs.orienworkx.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.hadoop.mapreduce.v2.api.protocolrecords.GetDelegationTokenRequest;
import org.codehaus.jettison.json.JSONObject;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.orienlabs.orienworkx.utils.License;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.mongodb.util.JSON;
import com.sugarcrm.ws.soap.Int;

import io.appium.java_client.remote.MobileCapabilityType;

@Component
@Configuration
@ComponentScan(basePackages = "org.orienlabs")
@PropertySource(value = { 	"config/application-default.properties", 
							"config/email.properties",
							"config/application-${env}.properties",
							"config/application-${lang}.properties",
							"config/application-${locale}.properties"}, ignoreResourceNotFound = true)
public class Config {

	static Environment env;
	static ThreadLocal<HashMap<String, Object>> localEnvs= new ThreadLocal<>();
	static ThreadLocal<Map<String, Object>> localCaps= new ThreadLocal<>();
	
	static AnnotationConfigApplicationContext ctx;
	static {
		ctx = new AnnotationConfigApplicationContext();
		ctx.register(Config.class);
		ctx.refresh();
		env = ctx.getEnvironment();
		//System.out.println(env.toString());
		
		if(SystemUtils.IS_OS_LINUX) System.setProperty("platform.type", "LIN");
		if(SystemUtils.IS_OS_MAC) System.setProperty("platform.type", "MAC");
		if(SystemUtils.IS_OS_WINDOWS) System.setProperty("platform.type", "WIN");
		
		if(SystemUtils.OS_ARCH.indexOf("64") >= 0) 
			System.setProperty("platform.arch", "x64"); 
		else 
			System.setProperty("platform.arch", "x86");
		
		License.postInfo();
	}
	
	static HashMap<String, Object> getLocalEnv()
	{
		if (localEnvs.get() == null)
		{
			//Add from default property sources
			HashMap<String, Object> localEnv= new HashMap<>();
			for(Iterator it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext(); ) {
	            org.springframework.core.env.PropertySource propertySource = (org.springframework.core.env.PropertySource) it.next();
	            if (propertySource instanceof MapPropertySource) {
	            	localEnv.putAll(((MapPropertySource) propertySource).getSource());
	            }
	        }
			
			localEnvs.set(localEnv);
		}
		
		return localEnvs.get();
	}

	public static String get(String property, String defaultValue) {
		if(System.getProperty(property) != null)
			return System.getProperty(property);
		else if(System.getenv().containsKey(property))
			return System.getenv(property);
		else if(getLocalEnv().containsKey(property))
			return getLocalEnv().get(property).toString();
		else
			return defaultValue;
	}

	public static void set(String property, String propertyValue) {
		getLocalEnv().put(property, propertyValue);
	}

	public static String resolve(String text) {
		
		for (int i = 0; i < 10; i++) {
			String textBefore= text;
			
			Set<String> tokens= getTokens(text);
			for (String token : tokens) {
				Object replacement= get(token, null);
				if(replacement != null)
					text= text.replace("${" + token + "}", replacement.toString());
			}
			
			if (textBefore.equals(text)) break;
		}
		
		return text;
	}
	
	static Set<String> getTokens(String in)
	{
		final String fromChars= "${";
		final String toChars= "}";
		
		Set<String> tokens= new HashSet<>();
		int occurances= StringUtils.countMatches(in, fromChars);
		
		int fromIndex= 0;
		for (int i = 0; i < occurances; i++) {
			fromIndex= in.indexOf(fromChars, fromIndex) + fromChars.length();
			int toIndex= in.indexOf(toChars, fromIndex);
			if(fromIndex >=0 && toIndex >=0)
				tokens.add(in.substring(fromIndex, toIndex));
		}
		
		return tokens;
	}
	
	public static File getReportFolder()
	{
		return new File(get("report.dir", "output-html").toString());
	}
	
	public static File getIconFolder()
	{
		return new File(Config.getReportFolder(),  "html/icons");
	}
	
	public static File getScreenshotFolder() {
		return new File(Config.getReportFolder(),  "scrnshot");
	}

	public static File getMobileAppFile() {
		return new File(get("mobile.apps", "").toString());
	}
	
	public static File getMobileDeviceFile() {
		return new File(get("mobile.devices", "").toString());
	}
	
	public static String getGridUrl() {
		return resolve("${gridUrl}");
	}
	
	public static String getBrowserName() {
		return resolve("${browserName}");
	}

	public static void setAll(Map<String, ?> allParameters) {
		for (Entry<String, ?> p : allParameters.entrySet()) {
			getLocalEnv().put(p.getKey(), p.getValue().toString());
		}
	}

	public static void setCapabilities(Map<String, Object> browserCaps) {
		localCaps.set(browserCaps);	
	}
	
	@SuppressWarnings("unchecked")
	public static DesiredCapabilities getCapabilities() throws Throwable {
		//If a capability file was specified, use it
		if(localCaps.get() != null)
			return new DesiredCapabilities(localCaps.get());	
		
		//Build capabilities
		HashMap<String, Object> caps= new HashMap<>();
		
		if(!Config.get("caps_json", "").isEmpty())
		{
			Map<String, ?> m= new ObjectMapper().readValue(Config.resolve(Config.get("caps_json", "")), Map.class);
			Config.setAll(m);
			return new DesiredCapabilities(m);
		}
		
		String browserName= Config.get("browserName", null);
		caps.put(CapabilityType.BROWSER_NAME, browserName);
		
		String version= Config.get("version", "");
		String browserVersion= Config.get("browserVersion", "");
		String deviceName= Config.get("deviceName", "");
		String platformName= Config.get("platformName", "");
		String platformVersion= Config.get("platformVersion", "");
		String platform= Config.get("platform", "");
		
		String seleniumVersion= Config.get("seleniumVersion", "");
		String appiumVersion= Config.get("appiumVersion", "");
		String mobileEmulation= Config.get("mobileEmulation", "");
		
		String browserResolution= Config.get("browserResolution", "");
		String recordVideo= Config.get("record_video", "");
		String deviceOrientation= Config.get("deviceOrientation", "");
		
		String newCommandTimeout= Config.get("newCommandTimeout", "120");
		String udid= Config.get("udid", "");
		String appActivity= Config.get("appActivity", "");
		String appPackage= Config.get("appPackage", "");
		
		if(!version.isEmpty()) caps.put(MobileCapabilityType.VERSION, version);
		if(!browserVersion.isEmpty()) caps.put(MobileCapabilityType.BROWSER_VERSION, browserVersion);
		if(!deviceName.isEmpty()) caps.put(MobileCapabilityType.DEVICE_NAME, deviceName);
		if(!platformName.isEmpty()) caps.put(MobileCapabilityType.PLATFORM_NAME, platformName);
		if(!platformVersion.isEmpty()) caps.put(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
		if(!platform.isEmpty()) caps.put(MobileCapabilityType.PLATFORM, platform);
		if(!seleniumVersion.isEmpty()) caps.put("seleniumVersion", seleniumVersion);
		if(!appiumVersion.isEmpty()) caps.put("appiumVersion", appiumVersion);
		if(!mobileEmulation.isEmpty()) caps.put("mobileEmulation", getMobileEmulation(mobileEmulation));
		if(!browserResolution.isEmpty()) caps.put("browserResolution", browserResolution);
		if(!recordVideo.isEmpty()) caps.put("record_video", recordVideo.toLowerCase());
		if(!deviceOrientation.isEmpty()) caps.put("deviceOrientation", deviceOrientation);
		
		if(!newCommandTimeout.isEmpty()) caps.put("newCommandTimeout", Integer.parseInt(newCommandTimeout));
		if(!udid.isEmpty()) caps.put("udid", udid);
		if(!appActivity.isEmpty()) caps.put("appActivity", appActivity);
		if(!appPackage.isEmpty()) caps.put("appPackage", appPackage);
		
		return new DesiredCapabilities(caps);
	}

	public static void setAll(File file) throws IOException {
		FileInputStream fileInput = new FileInputStream(file);
		Properties properties = new Properties();
		properties.load(fileInput);
		fileInput.close();
		
		for (Entry<Object, Object> prop : properties.entrySet())
			Config.set(prop.getKey().toString(), prop.getValue().toString());
	}
	
	private static HashMap<String, Object> getMobileEmulation(String mobileEmulation)
	{
		HashMap<String, Object> rawMap= new Gson().fromJson(mobileEmulation, HashMap.class);
		if(rawMap.containsKey("deviceMetrics"))
		{
			LinkedTreeMap deviceMetricesTreeMap= (LinkedTreeMap) rawMap.get("deviceMetrics");
			HashMap<String, Object> deviceMetrices= new HashMap<String, Object>();
			deviceMetrices.put("width", Integer.parseInt(deviceMetricesTreeMap.get("width").toString()));
			deviceMetrices.put("height", Integer.parseInt(deviceMetricesTreeMap.get("height").toString()));
			deviceMetrices.put("pixelRatio", Float.parseFloat(deviceMetricesTreeMap.get("pixelRatio").toString()));
			rawMap.put("deviceMetrics", deviceMetrices);
		}
		
		return rawMap;
	}
}
