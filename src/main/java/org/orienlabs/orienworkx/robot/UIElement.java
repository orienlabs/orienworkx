package org.orienlabs.orienworkx.robot;

import java.net.SocketException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.utils.HelperUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import lombok.extern.java.Log;

@Log
public class UIElement {

	String locatorType, locatorValue;
	WebElement e;
	public boolean lookInFrames= false;

	UIElement(String byWhat, String byHow) {
		locatorType = byWhat;
		locatorValue = byHow;
	}
	
	UIElement(String byWhat, String byHow, boolean lookInFrames) {
		locatorType = byWhat;
		locatorValue = byHow;
		this.lookInFrames= lookInFrames;
	}
	
	UIElement(WebElement e) {
		this.e= e;
	}

	public UIElement(String locator) {
		if(locator.startsWith("//")) locator= "xpath=" + locator;
		locatorType = locator.split("\\=")[0];
		locatorValue = locator.replace(locatorType + "=", "");
	}
	
	public static UIElement byXpath(String locator) {
		return new UIElement("xpath", locator);
	}

	public static UIElement byCSS(String locator) {
		return new UIElement("css", locator);
	}

	public static UIElement byName(String locator) {
		return new UIElement("name", locator);
	}

	public static UIElement byId(String locator) {
		return new UIElement("id", locator);
	}
	
	public static UIElement byAccessibilityId(String locator) {
		return new UIElement("accessibilityId", locator);
	}
	
	public static UIElement byAndroidUIAutomator(String locator) {
		return new UIElement("androidUIAutomator", locator);
	}
	
	public static UIElement byIOSUIAutomation(String locator) {
		return new UIElement("IOSUIAutomation", locator);
	}
	
	public static UIElement byWebElement(WebElement e) {
		return new UIElement(e);
	}

	public WebElement getElement() throws Throwable
	{
		return findElement();
	}
	
	WebElement findElement() throws Throwable {
		
		/*
		List<WebElement> elements= findElements();
		if(elements.size() == 1)
			return elements.get(0);
		
		if(elements.size() == 0)
			return UIDriver.getDriver().findElement(buildLocator());
		
		if(elements.size() >1)
		{
			for (WebElement element : elements)
				if(element.isDisplayed()) return element;
		}
		
		return elements.get(0);
		*/
		
		if(this.e != null)
			return e;
		
		try {
			UIDriver.getDriver().switchTo().defaultContent();
			return UIDriver.getDriver().findElement(buildLocator());
		} catch (SocketException e) {
			Thread.sleep(5000);
			return findElement();
		} catch (NoSuchElementException notFoundEx)
		{
			if(!lookInFrames)
				throw notFoundEx;
			else
			{
				List<WebElement> elements= findElements();
				if (elements.size() >0) return elements.get(0);
				throw notFoundEx;
			}
		}
	}
	
	public List<WebElement> findElements() throws Throwable {
		WebDriver w= UIDriver.getDriver();
		if(this.e != null)
			return Arrays.asList(new WebElement[] {this.e});
		
		List<WebElement> elements;
		try {
			w.switchTo().defaultContent();
			elements= w.findElements(buildLocator());
			
			//if property is set as look in frames, add all others
			if(elements.size() ==0 && lookInFrames)
			{
				try {
					List<WebElement> frames= w.findElements(By.tagName("iframe"));
					for (WebElement frame : frames) {
						w.switchTo().frame(frame);
						elements.addAll(w.findElements(buildLocator()));
						if (elements.size() > 0) break;
					}
				} finally {
					//w.switchTo().defaultContent();
				}
			}
			
		} catch (SocketException e) {
			Thread.sleep(5000);
			return findElements();
		}
		return elements;
	}

	By buildLocator() throws Throwable {
		switch (locatorType) {
		case "xpath":
			return By.xpath(locatorValue);
		case "name":
			return By.name(locatorValue);
		case "css":
			return By.cssSelector(locatorValue);
		case "id":
			return By.id(locatorValue);
		case "accessibilityId":
			return new MobileBy.ByAccessibilityId(locatorValue);
		case "androidUIAutomator":
			return new MobileBy.ByAndroidUIAutomator(locatorValue);
		case "IOSUIAutomation":
			return new MobileBy.ByIosUIAutomation(locatorValue);
		default:
			throw new Exception("Unknown locator type " + locatorType);
		}
	}
	
	public UIElement setText(String text) throws Throwable
	{
		findElement().sendKeys(text);
		return this;
	}
	
	public UIElement selectOptionByText(String text) throws Throwable
	{
		new Select(findElement()).selectByVisibleText(text);
		return this;
	}
	
	public UIElement selectOptionByValue(String text) throws Throwable
	{
		new Select(findElement()).selectByValue(text);
		return this;
	}
	
	public UIElement selectOptionByIndex(int index) throws Throwable
	{
		new Select(findElement()).selectByIndex(index);
		return this;
	}
	
	public UIElement clearText() throws Throwable
	{
		findElement().clear();
		return this;
	}
	
	public String getText() throws Throwable
	{
		return findElement().getText();
	}
	
	public String getAttribute(String attrName) throws Throwable {
		return findElement().getAttribute(attrName);
	}
	
	public void submit() throws Throwable
	{
		findElement().submit();
	}
	
	public UIElement waitFor(int timeout, Object intent) throws Throwable {
		if(intent == null)
			return waitFor(timeout);
		
		FluentWait<WebDriver> waitObj = new FluentWait<WebDriver>(UIDriver.getDriver())
											.withTimeout(timeout, TimeUnit.SECONDS).pollingEvery(1, TimeUnit.SECONDS)
											.ignoring(NoSuchElementException.class);
		try {
			switch (intent.toString()) {
			case "clickable":
				waitObj.until(ExpectedConditions.elementToBeClickable(buildLocator()));
				break;
			
			case "invisible":
				waitObj.until(ExpectedConditions.invisibilityOfElementLocated(buildLocator()));
				break;
				
			default:
				throw new Exception("Unknown intent " + intent);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return this;
	}
	
	public UIElement waitFor(int timeout, UIElement clickOn) throws Throwable {
		long start= System.currentTimeMillis();
		do {
			List<WebElement> elements= findElements();
			for (WebElement element : elements) {
				try {
					if(element.isDisplayed()) return this;
				} catch (Exception e) {}
			}
			
			try {
				if(clickOn != null)
					clickOn.click();
			} catch (Exception e) {}
			
		} while ((System.currentTimeMillis()- start) < (timeout * 1000));

		return this;
	}
	
	public UIElement waitFor(int timeout) throws Throwable {
		return waitFor(timeout, null);
	}
	
	public void waitForNot(int timeout, UIElement clickOn) throws Throwable {
		long start= System.currentTimeMillis();
		do {
			List<WebElement> elements = findElements();
			if (elements.size() == 0) return;

			boolean ret = true;
			for (WebElement element : elements) {
				try {
					if (element.isDisplayed()) {
						ret = false;
						try {
							if (clickOn != null) clickOn.click();
						} catch (Exception e) {}
					}
				} catch (Exception e) {
					ret = false;
				}
			}

			if (ret) return;

		} while ((System.currentTimeMillis()- start) < (timeout * 1000));
	}
	
	public void waitForNot(int timeout) throws Throwable {
		waitForNot(timeout, null);
	}
	
	public boolean isDisplayed() {
		try {
			return findElement().isDisplayed();
		} catch (Throwable ex) {
			//logger.debug(ex.getMessage());
			return false;
		}
	}
	
	public boolean isDisabled() throws Throwable {
		return !findElement().isEnabled();
	}
	
	public boolean isEnabled() throws Throwable {
		return findElement().isEnabled();
	}
	
	public void click() throws Throwable
	{
		e= findElement();
		try {
			this.moveTo(e);
		} catch (Exception e) {
		}
		
		if(UIDriver.getDriver() instanceof TouchDriver)
			((TouchDriver)UIDriver.getDriver()).tap(e);
		else if(UIDriver.getDriver() instanceof RemoteTouchDriver)
			((RemoteTouchDriver)UIDriver.getDriver()).tap(e);
		else if(UIDriver.getDriver() instanceof AndroidTouchDriver)
			((AndroidTouchDriver)UIDriver.getDriver()).tap(e);
		else if(UIDriver.getDriver() instanceof IOSDriver || UIDriver.getDriver() instanceof AppiumDriver)
		{
			String native_offset_x= Config.get("native_offset_x", "");
			String native_offset_y= Config.get("native_offset_y", "");
			
			if(!native_offset_x.isEmpty() && !native_offset_y.isEmpty())
			{
				Point p= this.getCenter();
				int x= p.x + Integer.parseInt(native_offset_x);
				int y= p.y + Integer.parseInt(native_offset_y);
				
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("x", x);
				params.put("y", y);
				log.info("Performing mobile: tap using " + params.toString());
				((JavascriptExecutor)UIDriver.getDriver()).executeScript("mobile: tap", params);
			}
			else
				e.click();
		}
		else if ("true".equalsIgnoreCase(Config.get("clickByCoordinates", "")))
		{
			log.info("Clicking using coordinates for element " + this.buildLocator());
			this.clickByCoordinates();
		}
		else if ("true".equalsIgnoreCase(Config.get("nativeWebTap", "")))
		{
			log.info("Clicking by moving to element " + this.buildLocator());
			this.clickAt();
		}
		else
		{
			try {
				findElement().click();
			} 
			catch (StaleElementReferenceException staleEx)
			{
				HelperUtil.sleepForSeconds(1);
				findElement().click();
			}
			catch (ElementNotInteractableException e) {
				//try {
					new Actions(UIDriver.getDriver()).moveToElement(findElement()).click().build().perform();
				//} catch (Exception e1) {
				//	throw e;
				//}
			}
		}
	}
	
	public void clickAt() throws Throwable
	{
		if(UIDriver.getDriver() instanceof IOSDriver 
				|| UIDriver.getDriver() instanceof AndroidDriver)
			new TouchAction((PerformsTouchActions) UIDriver.getDriver()).tap(findElement()).perform();
		else
		{
			WebElement e= findElement();
			new Actions(UIDriver.getDriver()).moveToElement(e).click().build().perform();
		}		
	}
	
	public void moveTo(WebElement e) throws Throwable
	{
		new Actions(UIDriver.getDriver()).moveToElement(e).build().perform();
	}
	
	public void clickByCoordinates() throws Throwable
	{
		if(UIDriver.getDriver() instanceof IOSDriver 
				|| UIDriver.getDriver() instanceof AndroidDriver)
			this.touch(Float.valueOf("0.5"), Float.valueOf("0.5"));
		else
		{
			Point p= this.getCenter();
			WebElement body= UIDriver.getDriver().findElement(By.tagName("body"));
			new Actions(UIDriver.getDriver()).moveToElement(body, p.getX(), p.getY()).click().build().perform();
		}		
	}
	
	/**
	 * Simulate touch action at a given offset percentage from top-left corner of element
	 * @param x_offset 0.10 for 10% from left
	 * @param y_offset 0.50 for middle of the element
	 * @throws Throwable
	 */
	public void touch(float x_offset, float y_offset) throws Throwable
	{
		WebElement e= getElement();
		Point loc= e.getLocation();
		Dimension size= e.getSize();
		int x= (int) (loc.x + (size.width * x_offset));
		int y= (int) (loc.y + (size.height* y_offset));
		
		if(UIDriver.getDriver() instanceof IOSDriver)
			new TouchActions(((IOSDriver)UIDriver.getDriver())).down(x, y).up(x, y).build().perform();
		else if(UIDriver.getDriver() instanceof AndroidDriver)
			new TouchActions(((AndroidDriver)UIDriver.getDriver())).down(x, y).up(x, y).build().perform();
		else if(UIDriver.getDriver() instanceof TouchDriver || UIDriver.getDriver() instanceof RemoteTouchDriver || UIDriver.getDriver() instanceof AppiumDriver)
		    new TouchActions(UIDriver.getDriver()).down(x, y).up(x, y).build().perform();
		else
			new Actions(UIDriver.getDriver()).moveToElement(e,0,0).moveByOffset(x, y).click().build().perform();
	}
	
	public Point getCenter() throws Throwable
	{
		WebElement e= findElement();
		Point l= e.getLocation();
		Dimension s= e.getSize();
		return new Point(l.x + s.width/2, l.y+ s.height/2);
	}
	
	public void tap() throws Throwable
	{
		click();
	}
	
	public void swipeLeft() throws Throwable {
		this.swipe("left");
	}
	
	public void swipeRight() throws Throwable {
		this.swipe("right");
	}
	
	public void swipeUp() throws Throwable {
		this.swipe("up");
	}
	
	public void swipeDown() throws Throwable {
		this.swipe("down");
	}
	
	public void swipe(String direction) throws Throwable {
		String currentContext= ((AppiumDriver<?>)UIDriver.getDriver()).getContext();
		((AppiumDriver<?>)UIDriver.getDriver()).context("NATIVE_APP");
		
		//Dimension d= UIDriver.getDeviceSize();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("direction", direction);
		params.put("element", ((RemoteWebElement) this.getElement()).getId());
		((JavascriptExecutor)UIDriver.getDriver()).executeScript("mobile: swipe", params);
		((AppiumDriver<?>)UIDriver.getDriver()).context(currentContext);
	}
	
}