package org.orienlabs.orienworkx.robot;

import java.net.URL;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.interactions.internal.Locatable;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteTouchScreen;
import org.openqa.selenium.remote.RemoteWebDriver;

import lombok.extern.java.Log;

@Log
public class RemoteTouchDriver extends RemoteWebDriver implements HasTouchScreen {
	private RemoteTouchScreen touch;

	public RemoteTouchDriver(URL url, Capabilities caps) {
		super(url, caps);
		touch = new RemoteTouchScreen(getExecuteMethod());
	}

	public TouchScreen getTouch() {
		return touch;
	}
	
	public void tap(WebElement e)
	{
		try {
			Coordinates c= ((Locatable)e).getCoordinates();
			c.inViewPort();
			getTouch().singleTap(((Locatable)e).getCoordinates());
		} catch (Exception e2) {
			e.click();
		}
	}
}