package org.orienlabs.orienworkx.robot;

import java.util.HashMap;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobileDevice {
	
	public MobileDevice() {
		deviceId= RandomStringUtils.randomAlphanumeric(10);
	}
	
	String deviceId; 
	
	@JsonProperty("mobileDevice")
	HashMap<String, Object> deviceProperties;
	
	public HashMap<String, Object> getProperties()
	{
		return deviceProperties;
	}
	
	public String getDeviceId()
	{
		return deviceId;
	}
}