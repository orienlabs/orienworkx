package org.orienlabs.orienworkx.robot;

import java.net.URL;
import java.util.HashMap;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.remote.RemoteTouchScreen;
import org.openqa.selenium.remote.RemoteWebElement;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.android.AndroidDriver;
import lombok.extern.java.Log;

@Log
public class AndroidTouchDriver extends AndroidDriver implements HasTouchScreen {
	private RemoteTouchScreen touch;

	public AndroidTouchDriver(URL url, Capabilities caps) {
		super(url, caps);
		touch = new RemoteTouchScreen(getExecuteMethod());
	}

	public TouchScreen getTouch() {
		return touch;
	}
	
	public void tap(WebElement e)
	{
		//new TouchActions(UIDriver.getDriver()).singleTap(e).build().perform();
		getTouch().singleTap(((Locatable)e).getCoordinates());
	}
	
	public void tap(int x, int y)
	{
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("x", x);
		params.put("y", y);
		log.info("Performing mobile: tap using " + params.toString());
		((JavascriptExecutor) UIDriver.getDriver()).executeScript("mobile: tap", params);
		//new TouchActions(UIDriver.getDriver()).down(x, y).up(x, y).build().perform();
	}
}