package org.orienlabs.orienworkx.robot;

import java.util.Set;

import org.openqa.selenium.WebElement;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PressesKeyCode;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import net.bytebuddy.implementation.bind.annotation.Super;

public class MobileUIDriver extends UIDriver {
	public static void startApp() throws Throwable
	{
		UIDriver.startBrowser();
	}
	
	public static void startSettingsApp()
	{}
	
	public static void pressKey(int keycode)
	{}
	
	public static void pressEnter()
	{
		((AndroidDriver)getDriver()).pressKeyCode(AndroidKeyCode.ENTER);
	}
	
	public static void goBack()
	{}
	
	public static void goHome()
	{
		((AndroidDriver) getDriver()).pressKeyCode(AndroidKeyCode.HOME);
	}
	
	public static void resetPermissions()
	{}
	
	public static void grantPermission()
	{}
	
	public static void revokePermission()
	{}
	
	public static void removeApp()
	{}
	
	public static void resetApp()
	{}
	
	public static void closeAll()
	{}
	
	public static String getDeviceId() throws Throwable
	{
		return null;
	}
	
	public static String getPlatform() throws Throwable
	{
		return null;
	}
	
	public static String getPlatformVersion() throws Throwable
	{
		return null;
	}
	
	public static String getDeviceModel() throws Throwable
	{
		return null;
	}
	
	public static void swipeLeft() throws Throwable {
		int height = getHeight();
		int width = getWidth();

		swipe((int) (width * 0.85), height / 2, (int) (width * 0.15), height / 2);
	}
	
	public static void swipeRight() throws Throwable {
		int height = getHeight();
		int width = getWidth();

		swipe((int) (width * 0.15), height / 2, (int) (width * 0.85), height / 2);
	}
	
	public static void swipeUp() throws Throwable {
		int height = getHeight();
		int width = getWidth();

		swipe(width / 2, (int) (height * 0.80), width / 2, (int) (height * 0.20));
	}
	
	public static void swipeDown() throws Throwable {
		int height = getHeight();
		int width = getWidth();

		swipe(width / 2, (int) (height * 0.15), width / 2, (int) (height * 0.85));
	}
	
	@SuppressWarnings("rawtypes")
	public static void swipe(int startx, int starty, int endx, int endy) throws Throwable {
		int xOffset = endx - startx;
		int yOffset = endy - starty;

		new TouchAction((MobileDriver) currentDriver.get()).press(startx, starty).moveTo(xOffset, yOffset).release().perform();
	}
	
	@SuppressWarnings("rawtypes")
	public static void tap(int x, int y) throws Throwable {
		new TouchAction((MobileDriver) getDriver()).press(x, y).release().perform();
	}
	
	@SuppressWarnings("rawtypes")
	public static void switchToWebView() throws Throwable {
		Set<String> contexts = ((MobileDriver) getDriver()).getContextHandles();
		for (int i = 0; i < 10; i++) {
			for (String context : contexts) {
				if (context.contains("WEBVIEW")) {
					((MobileDriver) getDriver()).context(context);
					return;
				}
			}
			Thread.sleep(2);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static void switchToNativeView() throws Throwable {
		Set<String> contexts = ((MobileDriver) getDriver()).getContextHandles();
		for (String context : contexts) {
			if (context.contains("NATIVE")) {
				((MobileDriver) getDriver()).context(context);
				break;
			}
		}
	}
	
	public static void hideKeyboard() {
		try {
			((MobileDriver) getDriver()).hideKeyboard();
		} catch (Throwable ex) {
			//logger.debug("Problem in Hiding keyboard due to: " + ex.getMessage());
		}
	}

	public static void startActivity(Activity a) {
		System.out.println("String activity " + a.getAppPackage() + a.getAppActivity());
		((AndroidDriver)getDriver()).startActivity(a);
	}

	public static void startActivity(String appPackage, String appActivity) {
		startActivity(new Activity(appPackage, appActivity));
	}
}
