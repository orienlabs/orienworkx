package org.orienlabs.orienworkx.robot;

import java.util.List;

import org.orienlabs.orienworkx.config.Config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class MobileApps {
	
	@JsonProperty("mobileApps")
	public List<MobileApp> apps;
	
	static MobileApps thisInstance;
	
	static {
		ObjectMapper testYamlMapper = new ObjectMapper(new YAMLFactory());
		try {
			thisInstance= testYamlMapper.readValue(Config.getMobileAppFile(), MobileApps.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getApp(String platform, String appName)
	{
		for (MobileApp app : thisInstance.apps) {
			System.out.println(app.getDeviceId());
			System.out.println(app.getCapabilities());
		}
	}
}