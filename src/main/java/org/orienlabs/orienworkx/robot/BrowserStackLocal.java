package org.orienlabs.orienworkx.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.orienlabs.orienworkx.config.Config;
import com.browserstack.local.Local;
import lombok.extern.java.Log;

@Log
public class BrowserStackLocal {
	static ThreadLocal<Local> currentLocal= new ThreadLocal<>();
	static ThreadLocal<LinkedList<Local>> knownLocals= new ThreadLocal<>();
	static List<Local> allLocals= new ArrayList<>();
	
	static{
		//Ensure all drivers are closed
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				for (Local local : allLocals) {
					try {
						local.stop();
					} catch (Exception e) {}
				}
			}
		}));
	}
	
	public static synchronized void setLocal(Local l)
	{
		currentLocal.set(l);
		
		if(l != null && !allLocals.contains(l)) allLocals.add(l);
		
		if(knownLocals.get() == null) knownLocals.set(new LinkedList<>());
		if(!knownLocals.get().contains(l) && l != null)  knownLocals.get().add(l);
	}
	
	public static Local getLocal()
	{
		return currentLocal.get();
	}
	
	public static String start(String key) throws Throwable
	{
		if(getLocal() != null)
			if(getLocal().isRunning()) return Config.get("cloudTunnerIdentifier", "");
		
		String cloudTunnerIdentifier= RandomStringUtils.randomAlphabetic(10);
		Local bsLocal = new Local();

		HashMap<String, String> bsLocalArgs = new HashMap<String, String>();
		bsLocalArgs.put("key", key);
		bsLocalArgs.put("forcelocal", "true");
		bsLocalArgs.put("localIdentifier", cloudTunnerIdentifier);
		
		bsLocal.start(bsLocalArgs);
		Config.set("cloudTunnerIdentifier", cloudTunnerIdentifier);
		System.out.println(bsLocal.isRunning());
		setLocal(bsLocal);
		
		return cloudTunnerIdentifier;
	}
	
	public static void quit()
	{
		try {
			getLocal().stop();
		} catch (Exception e) 
		{}
		
		setLocal(null);
	}
}