package org.orienlabs.orienworkx.robot;

import java.util.HashMap;

/**
 * @author kumar.anil
 *
 */

public class DeviceEmulation {
	static HashMap<String, Object[]> devices= new HashMap<>();
	static{
		//devices.put("Nexus 6P", new Object[]{412, 732, 3.5, "Mozilla/5.0 (Linux; Android 6.0.1; Nexus 6P Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36"});
		//devices.put("Galaxy Note 3", new Object[]{360, 640, 3.0, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36"});
		//devices.put("Nexus 10", new Object[]{800, 1280, 2.0, "Mozilla/5.0 (Linux; Android 4.3; Nexus 10 Build/JSS15Q) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36"});
		//devices.put("iPhone 6", new Object[]{375, 667, 2.0, "Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"});
		//devices.put("iPad Pro", new Object[]{1024, 1366, 2.0, "Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1"});
		//devices.put("iPhone 6", new Object[]{412, 732, 3.5, ""});
	}
	
	public static Object[] getDetails(String deviceType)
	{
		return devices.get(deviceType);
	}
}
