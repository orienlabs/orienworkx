package org.orienlabs.orienworkx.robot;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.HasTouchScreen;
import org.openqa.selenium.interactions.Locatable;
import org.openqa.selenium.interactions.TouchScreen;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.RemoteTouchScreen;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;

public class TouchDriver extends ChromeDriver implements HasTouchScreen {
	private RemoteTouchScreen touch;

	public TouchDriver(ChromeOptions chromeOptions) {
		super(chromeOptions);
		try {
			this.manage().window().maximize();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		touch = new RemoteTouchScreen(getExecuteMethod());
	}

	public TouchScreen getTouch() {
		return touch;
	}
	
	public void tap(WebElement e)
	{
		try {
			Coordinates c= ((Locatable)e).getCoordinates();
			c.inViewPort();
			getTouch().singleTap(((Locatable)e).getCoordinates());
		} catch (Exception e2) {
			e.click();
		}
	}
}