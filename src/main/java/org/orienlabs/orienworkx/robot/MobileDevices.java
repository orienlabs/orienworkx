package org.orienlabs.orienworkx.robot;

import java.util.List;

import org.orienlabs.orienworkx.config.Config;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class MobileDevices {
	
	@JsonProperty("mobileDevices")
	public List<MobileDevice> devices;
	
	static MobileDevices thisInstance;
	
	static {
		ObjectMapper testYamlMapper = new ObjectMapper(new YAMLFactory());
		try {
			thisInstance= testYamlMapper.readValue(Config.getMobileDeviceFile(), MobileDevices.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void getDevice(String platform)
	{
		for (MobileDevice device : thisInstance.devices) {
			System.out.println(device.getDeviceId());
			System.out.println(device.getProperties());
		}
	}
}
