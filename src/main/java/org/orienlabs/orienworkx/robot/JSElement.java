package org.orienlabs.orienworkx.robot;

import org.openqa.selenium.WebElement;

import lombok.extern.java.Log;

@Log
/*
 * This element class performs all actions on elements on UI using JavaScript
 */
public class JSElement extends UIElement {

	JSElement(String byWhat, String byHow) {
		super(byWhat, byHow);
	}
	
	JSElement(String byWhat, String byHow, boolean lookInFrames) {
		super(byWhat, byHow, lookInFrames);
	}
	
	JSElement(WebElement e) {
		super(e);
	}

	public JSElement(String locator) {
		super(locator);
	}
	
	public static JSElement byXpath(String locator) {
		return new JSElement("xpath", locator);
	}

	public static JSElement byCSS(String locator) {
		return new JSElement("css", locator);
	}

	public static JSElement byName(String locator) {
		return new JSElement("name", locator);
	}

	public static JSElement byId(String locator) {
		return new JSElement("id", locator);
	}
	
	public static JSElement byAccessibilityId(String locator) {
		return new JSElement("accessibilityId", locator);
	}
	
	public static JSElement byAndroidUIAutomator(String locator) {
		return new JSElement("androidUIAutomator", locator);
	}
	
	public static JSElement byIOSUIAutomation(String locator) {
		return new JSElement("IOSUIAutomation", locator);
	}
	
	public static JSElement byWebElement(WebElement e) {
		return new JSElement(e);
	}
	
	public JSElement setText(String text) throws Throwable
	{
		UIDriver.executeScript("arguments[0].value=\"" + text + "\";", new Object[] {this.getElement()});
		return this;
	}
	
	public JSElement clearText() throws Throwable
	{
		return this.setText("");
	}
	
	public String getText() throws Throwable
	{
		return UIDriver.executeScript("return arguments[0].value;", new Object[] {this.getElement()});
	}
	
	public void click() throws Throwable
	{
		UIDriver.executeScript("return arguments[0].click();", new Object[] {this.getElement()});
	}
}