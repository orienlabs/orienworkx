package org.orienlabs.orienworkx.robot;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.logger.Logger;
import org.orienlabs.orienworkx.robot.AndroidTouchDriver;
import org.orienlabs.orienworkx.robot.PhantomJSFixedDriver;
import org.orienlabs.orienworkx.robot.RemoteTouchDriver;
import org.orienlabs.orienworkx.robot.TouchDriver;
import org.orienlabs.orienworkx.utils.HelperUtil;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.OperaDriverManager;
import io.github.bonigarcia.wdm.PhantomJsDriverManager;
import lombok.extern.java.Log;

@Log
public class UIDriver {
	static ThreadLocal<WebDriver> currentDriver= new ThreadLocal<>();
	static ThreadLocal<LinkedList<WebDriver>> knownDrivers= new ThreadLocal<>();
	static ThreadLocal<Dimension> deviceSize= new ThreadLocal<>();
	static ThreadLocal<Capabilities> actualCaps= new ThreadLocal<>();
	static List<WebDriver> allDrivers= new ArrayList<>();
	
	static{
		//knownDrivers.set(new LinkedList<>());
		
		//Ensure all drivers are closed
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				for (WebDriver webDriver : allDrivers) {
					try {
						webDriver.quit();
					} catch (Exception e) {}
				}
			}
		}));
	}
	
	public static synchronized void setDriver(WebDriver w)
	{
		currentDriver.set(w);
		
		if(w != null && !allDrivers.contains(w)) allDrivers.add(w);
		
		if(knownDrivers.get() == null) knownDrivers.set(new LinkedList<>());
		if(!knownDrivers.get().contains(w) && w != null)  knownDrivers.get().add(w);
	}
	
	public static WebDriver getDriver()
	{
		return currentDriver.get();
	}
	
	public static void switchByIndex(int index)
	{
		currentDriver.set(knownDrivers.get().get(index));
	}
	
	public static void switchByContext(String context)
	{
		throw new NotImplementedException("");
	}
	
	public static void switchByTitle(String title, boolean partialMatch)
	{
		throw new NotImplementedException("");
	}
	
	public static void switchByUrl(String url, boolean partialMatch)
	{
		throw new NotImplementedException("");
	}
	
	public static void startBrowser() throws Throwable
	{
		startBrowser(Config.getGridUrl(), Config.getCapabilities());
	}
	
	public static void startBrowser(String gridUrl, DesiredCapabilities caps) throws Throwable
	{
		RemoteWebDriver d= null;
		String browserName= caps.getBrowserName(); //System.out.println(gridUrl);
		Object mobileEmulation= caps.getCapability("mobileEmulation");
		String userAgent= Config.get("userAgent", "");
		String isHeadless=Config.get("headless",  "");
		
		String browserResolution= Objects.toString(caps.getCapability("browserResolution"),"");
		String browserWidth= "";
		String browserHeight= "";
		if(!browserResolution.isEmpty())
		{
			browserWidth= browserResolution.split(Pattern.quote("x"))[0];
			browserHeight= browserResolution.split(Pattern.quote("x"))[1];
		}
		
		boolean startMaximized	= false;
		if(mobileEmulation != null || browserResolution.isEmpty()) startMaximized= true;
		
		try {
			switch (browserName) {
			case BrowserType.FIREFOX:
			case "Firefox":
				caps.setCapability("acceptInsecureCerts", true);
				FirefoxOptions ffOptions= new FirefoxOptions(caps);
				if(!userAgent.isEmpty())
					ffOptions.addPreference("general.useragent.override", userAgent);
				
				if(isHeadless.equalsIgnoreCase("true"))
					ffOptions.setHeadless(true);
				
				if(gridUrl.isEmpty())
				{
					String driverVersion= Config.get("driver.firefox.version", "");
					if(driverVersion.isEmpty()) FirefoxDriverManager.firefoxdriver().setup();
					else FirefoxDriverManager.firefoxdriver().version(driverVersion).setup();
					
					d= new FirefoxDriver(ffOptions);
				}
				else
					caps.setCapability(FirefoxOptions.FIREFOX_OPTIONS, ffOptions);
				break;
				
			case BrowserType.PHANTOMJS:
			case "phantom":
				if(gridUrl.isEmpty())
				{
					String driverVersion= Config.get("driver.phantom.version", "");
					if(driverVersion.isEmpty()) PhantomJsDriverManager.phantomjs().setup();
					else PhantomJsDriverManager.phantomjs().version(driverVersion).setup();
					
					//PhantomJsDriverManager.phantomjs().version("1.9.1").setup();
					//PhantomJsDriverManager.getInstance().version(Config.get("driver.phantom.version", "LATEST")).setup();
					if(!userAgent.isEmpty())
						caps.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent", userAgent);
					d= new PhantomJSFixedDriver(caps);
				}
				break;
				
			case BrowserType.CHROME:
			case "Chrome":
			case "chrome_headless":
			case "chrome-headless":
				ChromeOptions chromeOptions = new ChromeOptions();
				chromeOptions.merge(caps);
				chromeOptions.setPageLoadStrategy(PageLoadStrategy.NONE);
				chromeOptions.addArguments(
						Arrays.asList(
								"--disable-extensions",
								"--disable-browser-side-navigation",
								"--disable-gpu",
								"--incognito",
								"enable-automation",
								"--window-size=1920,1080",
								"--dns-prefetch-disable",
								"--force-device-scale-factor=1",
								"test-type",
								"no-default-browser-check", 
								"ignore-certificate-errors",
								"disable-infobars"
								));
				
				//if(startMaximized)
				//	chromeOptions.addArguments(Arrays.asList("start-maximized"));
				
				if(!userAgent.isEmpty())
					chromeOptions.addArguments(Arrays.asList("--user-agent=" + userAgent));
				
				if(!browserWidth.isEmpty())
					chromeOptions.addArguments(Arrays.asList("--window-size=" + browserWidth + "," + browserHeight));
				
				if(isHeadless.equalsIgnoreCase("true"))
					chromeOptions.addArguments(Arrays.asList("--headless"));
				
				if(mobileEmulation != null)
				{
					chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
					//chromeOptions.addArguments(Arrays.asList("auto-open-devtools-for-tabs"));
				}
				
				if(gridUrl.isEmpty())
				{
					String driverVersion= Config.get("driver.chrome.version", "LATEST");
					System.out.println("Setting up chrome driver version " + driverVersion);
					ChromeDriverManager.chromedriver().version(driverVersion).setup();
					if(mobileEmulation != null)
					{
						d= new TouchDriver(chromeOptions);
					}
					else
						d= new ChromeDriver(chromeOptions);
				}
				else
					caps.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				
				break;
				
			case BrowserType.EDGE:
			case "Edge":
				if(gridUrl.isEmpty())
				{
					EdgeDriverManager.edgedriver().version(Config.get("driver.edge.version", "LATEST")).setup();
					d= new EdgeDriver();
				}
				break;
				
			case BrowserType.HTMLUNIT:
				caps.merge(DesiredCapabilities.htmlUnit());
				break;
				
			case BrowserType.IE:
			case BrowserType.IEXPLORE:
			case "ie":
			case "Internet Explorer":
				caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
				if(gridUrl.isEmpty())
				{
					InternetExplorerDriverManager.iedriver().version(Config.get("driver.ie.version", "LATEST")).setup();
					d= new InternetExplorerDriver();
				}
				break;
				
			case BrowserType.OPERA_BLINK:
			case BrowserType.OPERA:
				if(gridUrl.isEmpty())
				{
					OperaDriverManager.operadriver().version(Config.get("driver.opera.version", "LATEST")).setup();
					d= new OperaDriver();
				}
				break;
				
			case BrowserType.SAFARI:
			case "Safari":
				SafariOptions safariOptions= new SafariOptions();
				safariOptions.merge(caps);
				
				if(gridUrl.isEmpty())
					d= new SafariDriver(safariOptions);
				break;
			
			case "iPhone":
			case "iPad":
			case "android":
			case "Android":
			case "iOS":
				break;
				
			case "":
				break;
				
			default:
				throw new UnknownError("Undefined browser " + browserName);
			}
		} catch (Exception e) {
			if (d != null) d.quit();
			throw e;
		}
		finally {
			setDriver(d);
		}
		
		
		String platForm= "";
		if(!gridUrl.isEmpty())
		{
			if(gridUrl.contains("browserstack.com") 
					&& Objects.toString(caps.getCapability("browserstack.local"),"").equalsIgnoreCase("true"))
			{
				String tunnel= BrowserStackLocal.start(Config.get("BROWSERSTACK_ACCESS_KEY", ""));
				caps.setCapability("browserstack.localIdentifier", tunnel);
			}
			
			platForm= ((caps.getCapability("platformName") == null) ? "" : caps.getCapability("platformName").toString());
			log.info("Launch remote browser at " + gridUrl + " using " + caps.toString());
			
			for (int i = 0; i < 50; i++) {
				try {
					/*with mobile emulation, always use RemoteWebDriver*/
					if(caps.getCapability("mobileEmulation") != null)
						d = new RemoteTouchDriver(new URL(gridUrl), caps);
					
					/*when platform is android and browser is specified, it generally means a mobile web*/
					else if(platForm.equalsIgnoreCase("android") 
							&& !StringUtils.isBlank(Objects.toString(caps.getCapability(MobileCapabilityType.BROWSER_NAME), "")))
						d = new AndroidTouchDriver(new URL(gridUrl), caps);
						
					/*when platform is iOS and browser is specified, it generally means a mobile web*/
					else if (platForm.equalsIgnoreCase("ios") 
							&& !StringUtils.isBlank(Objects.toString(caps.getCapability(MobileCapabilityType.BROWSER_NAME),""))
							&& (caps.getCapability("realMobile") == null))
					{
						if(caps.getCapability("nativeWebTap") == null) caps.setCapability("nativeWebTap", true);
							d = new RemoteTouchDriver(new URL(gridUrl), caps);
					}
					
					/*when device is android and device name is also specified, it generally means a native appium instance*/
					else if(platForm.equalsIgnoreCase("android") 
							&&  (!StringUtils.isBlank(Objects.toString(caps.getCapability(MobileCapabilityType.DEVICE_NAME),""))))
						d = new AndroidDriver(new URL(gridUrl), caps);
					
					/*when device is iOS and device UDID is also specified, it generally means a native appium instance*/
					else if(platForm.equalsIgnoreCase("ios") 
							&& (caps.getCapability(MobileCapabilityType.UDID) != null || caps.getCapability("realMobile") != null))
					{
						d = new IOSDriver(new URL(gridUrl), caps);
					}
					else
						d= new RemoteWebDriver(new URL(gridUrl), caps);
					
					setDriver(d);
					break;
				} catch (Exception e) {
					if (d != null) d.quit();
					if(
						e.getMessage().contains("Your team has reached its maximum number of parallel automated tests")
						||
						e.getMessage().contains("All parallel tests are currently in use, including the queued tests.")
					  )
					{
						HelperUtil.sleepForSeconds(60);
						continue;
					}
					else
						throw e;
				}
				
			}
			
			if(d instanceof IOSDriver || d instanceof AndroidDriver || d instanceof AppiumDriver)
			{
				String currentContext= ((AppiumDriver)d).getContext();
				log.info(((AppiumDriver)d).getContextHandles().toString());
				try {
					((AppiumDriver)d).context("NATIVE_APP");
					deviceSize.set(d.manage().window().getSize());
				} catch (Exception e) {
					log.severe("Exception while getting window size:");
					log.severe(e.getMessage());
				}
				finally {
					((AppiumDriver)d).context(currentContext);
				}
			}
			
			actualCaps.set(d.getCapabilities());
			log.info(actualCaps.get().toString());
		}
		
		//Set browser resolution
		if(platForm.equalsIgnoreCase("iOS") || platForm.equalsIgnoreCase("Android"))
			startMaximized= false;
		
		try {
			if(startMaximized /* && !browserName.toLowerCase().contains("chrome") */) d.manage().window().maximize();
			else if(!browserResolution.isEmpty()) 
			{
				String[] res= browserResolution.toString().split("x");
				d.manage().window().setSize(new Dimension(Integer.parseInt(res[0]), Integer.parseInt(res[1])));
			}
		} catch (Exception e) {
			log.warning(e.getMessage());
		}
		
		Config.set("sessionId", ((RemoteWebDriver) d).getSessionId().toString());
	}
	
	public static void navigate(String url) throws Throwable
	{
		log.info("Navigating to " + url);
		getDriver().get(url);
	}
	
	public static void refresh()
	{
		getDriver().navigate().refresh();
	}
	
	public static void quit()
	{
		try {
			getDriver().quit();
		} catch (Exception e) 
		{}
		
		try {
			BrowserStackLocal.quit();
		} catch (Exception e) 
		{}
		
		if(knownDrivers.get() != null)
		{
			for (WebDriver w : knownDrivers.get()) {
				try {
					w.quit();
				} catch (Exception e) {
				}
			}
			knownDrivers.get().clear();
		}
		setDriver(null);
	}

	public static File takePngScreenshot() throws Throwable {
		return takePngScreenshot(false);
	}
	
	public static File takePngScreenshot(boolean attachToReport) throws Throwable {
		//String gridUrl= Config.getGridUrl();
		File scrnshot= null;
		for (int i = 0; i < 10; i++) {
			try {
				scrnshot= HelperUtil.copyScreenshotToLocal(
						(File) ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE));
				break;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(attachToReport && scrnshot != null)
			Logger.attachScreenshot(scrnshot);
		
		return scrnshot;// return null;
	}
	
	public static void takePngScreenshot(HashMap<File, String> screenshots) throws Throwable {
		screenshots.put(takePngScreenshot(), getTitle() + ">" + getUrl());
	}
	
	public static byte[] getScreenshotAsBytes() {
		return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
	}

	public static File getSource() throws Throwable {
		return getSource(false);
	}
	
	public static File getPageSource() throws Throwable {
		return getSource();
	}
	
	public static String getPageText() throws Throwable {
		return getDriver().findElement(By.xpath("//*")).getText();
	}
	
	public static File getSource(boolean attachToReport) throws Throwable {
		File outputFile = null;
			String source = getDriver().getPageSource();
			if (source.startsWith("<?xml"))
				outputFile = File.createTempFile("src_", ".xml");
			else
				outputFile = File.createTempFile("src_", ".html");

			FileUtils.writeStringToFile(outputFile, source);

			//if (attachToReport)
			//	Reporter.addStepLog("<a href='" + outputFile.getAbsolutePath() + "'>page source</a>");

			return outputFile;
	}
	
	public static void getSource(HashMap<File, String> screenshots) throws Throwable {
		File srcFile= getSource(false);
		screenshots.put(srcFile, getTitle() + ">" + getUrl());	
	}

	public static String getTitle() {
		return getDriver().getTitle();
	}
	
	public static void goBack()
	{
		getDriver().navigate().back();
	}
	
	public static Dimension getSize() throws Throwable {
		return getDriver().manage().window().getSize();
	}
	
	public static Dimension getDeviceSize() throws Throwable {
		return deviceSize.get();
	}
	
	public static Capabilities getCapabilities() throws Throwable {
		return actualCaps.get();
	}
	
	public static int getHeight() throws Throwable {
		return getSize().getHeight();
	}
	
	public static int getWidth() throws Throwable {
		return getSize().getWidth();
	}

	public static Map<String, ?> getActualCapabilities() {
		return ((RemoteWebDriver)getDriver()).getCapabilities().asMap();
	}

	public static String getUrl() {
		return getDriver().getCurrentUrl();
	}

	public static String executeScript(String script) throws Throwable{
		return executeScript(script, new Object[] {});
	}
	
	public static String executeScript(String script, Object... elements) throws Throwable{

		Object ret = null;
		try {
			if (elements.length > 0 || script.contains("arguments") || script.contains("document")
					|| script.contains("return"))
				ret = ((JavascriptExecutor) getDriver()).executeScript(script, elements);
			else {
				ScriptEngineManager factory = new ScriptEngineManager();
				ScriptEngine engine = factory.getEngineByName("JavaScript");
				ret = engine.eval(script);
			}
		} catch (Throwable e) {
			throw e;
		}

		if (ret == null)
			return null;
		else
			return ret.toString();
	}
	
	public static void swipeLeft() throws Throwable {
		swipe("left");
	}
	
	public static void swipeRight() throws Throwable {
		swipe("right");
	}
	
	public static void swipeUp() throws Throwable {
		swipe("up");
	}
	
	public static void swipeDown() throws Throwable {
		swipe("down");
	}
	
	public static void swipe(String direction) throws Throwable {
		String currentContext= ((AppiumDriver<?>)UIDriver.getDriver()).getContext();
		((AppiumDriver<?>)UIDriver.getDriver()).context("NATIVE_APP");
		
		//Dimension d= UIDriver.getDeviceSize();
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("direction", direction);
		((JavascriptExecutor)UIDriver.getDriver()).executeScript("mobile: swipe", params);
		((AppiumDriver<?>)UIDriver.getDriver()).context(currentContext);
	}
	
	public static void clickAt(int x, int y)
	{
		log.info("Clicking at x:" + x + "; y:" + y);
		WebDriver d = UIDriver.getDriver();

		if (d instanceof TouchDriver)
			new Actions(d).moveByOffset(x, y).click().build().perform();
		else if (d instanceof RemoteTouchDriver)
			new Actions(d).moveByOffset(x, y).click().build().perform();
		else if (d instanceof AndroidTouchDriver)
			((AndroidTouchDriver)d).tap(x,y);
		else if (d instanceof IOSDriver || d instanceof AppiumDriver) {
			String native_offset_x = Config.get("native_offset_x", "");
			String native_offset_y = Config.get("native_offset_y", "");
			if (!native_offset_x.isEmpty() && !native_offset_y.isEmpty()) {
				native_offset_x = "0";
				native_offset_y = "0";
			}
			x = x + Integer.parseInt(native_offset_x);
			y = x + Integer.parseInt(native_offset_y);

			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("x", x);
			params.put("y", y);
			log.info("Performing mobile: tap using " + params.toString());
			((JavascriptExecutor) UIDriver.getDriver()).executeScript("mobile: tap", params);
		}
		else
			new Actions(d).moveByOffset(x, y).click().build().perform();
	}
}