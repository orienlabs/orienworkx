package org.orienlabs.orienworkx.robot;

import java.util.HashMap;

import org.apache.commons.lang3.RandomStringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MobileApp {
	
	public MobileApp() {
		deviceId= RandomStringUtils.randomAlphanumeric(10);
	}
	
	String deviceId; 
	
	@JsonProperty("mobileApp")
	HashMap<String, Object> caps;
	
	public HashMap<String, Object> getCapabilities()
	{
		return caps;
	}
	
	public String getDeviceId()
	{
		return deviceId;
	}
}
