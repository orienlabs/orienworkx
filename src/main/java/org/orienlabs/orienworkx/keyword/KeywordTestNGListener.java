package org.orienlabs.orienworkx.keyword;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.logger.AbstractListener;
import org.orienlabs.orienworkx.logger.ExtentManager;
import org.orienlabs.orienworkx.logger.AbstractListener.ExecutionMethod;
import org.orienlabs.orienworkx.utils.HelperUtil;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;

public class KeywordTestNGListener extends AbstractListener implements IReporter {
	
	static{
		System.setProperty("org.freemarker.loggerLibrary", "none");
		setExecutionMethod(ExecutionMethod.KEYWORD);
	}
	
	static HashMap<String, ExtentTest> parentTests= new HashMap<>();
    static HashMap<ITestResult, ExtentTest> tests= new HashMap<>();
    
	@Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {        
        /*
		for (ISuite suite : suites) {
            Map<String, ISuiteResult> result = suite.getResults();
            
            for (ISuiteResult r : result.values()) {
                ITestContext context = r.getTestContext();
                
                buildTestNodes(context.getFailedTests(), Status.FAIL);
                buildTestNodes(context.getSkippedTests(), Status.SKIP);
                buildTestNodes(context.getPassedTests(), Status.PASS);
            }
        }
        
        for (String s : Reporter.getOutput()) {
            extent.setTestRunnerOutput(s);
        }
        */
        ExtentManager.flush();
    }
	
	@Override
	public synchronized void onStart(ITestContext context) {
		super.onStart(context);
	}

	@Override
	public synchronized void onFinish(ITestContext context) {
		super.onFinish(context);
		ExtentManager.flush();
	}
	
	@Override
	public synchronized void onTestStart(ITestResult result) {
		super.onTestStart(result);
		//Map<String, String> params= result.getTestContext().getCurrentXmlTest().getAllParameters();
		//for (Entry<String, String> param : params.entrySet()) {
		//	test.get().assignCategory(param.getValue());
		//}
	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {
		super.onTestSuccess(result);
		getTest(result).pass("Test passed");
		ExtentManager.flush();
	}

	@Override
	public synchronized void onTestFailure(ITestResult result) {
		super.onTestFailure(result);
		getTest(result).fail(result.getThrowable());
		ExtentManager.flush();
	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {
		super.onTestSkipped(result);
		getTest(result).skip(result.getThrowable());
		ExtentManager.flush();
	}

	@Override
	public synchronized void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		super.onTestFailedButWithinSuccessPercentage(result);
		ExtentManager.flush();
	}
    
    @SuppressWarnings("unused")
	private void buildTestNodes(IResultMap tests, Status status) {
        if (tests.size() > 0) {
            for (ITestResult result : tests.getAllResults()) {
                ExtentTest test = getTest(result);
                
                for (String group : result.getMethod().getGroups())
                    test.assignCategory(group);

                if (result.getThrowable() != null) {
                    test.log(status, result.getThrowable());
                }
                else {
                    test.log(status, "Test " + status.toString().toLowerCase() + "ed");
                }
                
                test.getModel().setStartTime(getTime(result.getStartMillis()));
                test.getModel().setEndTime(getTime(result.getEndMillis()));
            }
        }
    }
    
    private Date getTime(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return calendar.getTime();      
    }
	
    @Override
	public void onStart(ISuite suite) {
    	super.onStart(suite);
	}
    
	@Override
	public void onFinish(ISuite suite) {
		super.onFinish(suite);
	}
	
	static synchronized ExtentTest getParentTest(ITestResult testResult)
	{
		String testGroupName= "";
		if(testResult.getAttribute("testGroupName") == null)
			testGroupName= testResult.getTestContext().getName();
		else
			testGroupName= testResult.getAttribute("testGroupName").toString();
			
		String suiteName= testResult.getTestContext().getSuite().getName();
		String qualifierName= suiteName + "." + testGroupName;
		
		if(!parentTests.containsKey(qualifierName))
		{
			ExtentTest test = ExtentManager.getInstance().createTest(testGroupName);
			parentTests.put(qualifierName, test);
        }
		
		return parentTests.get(qualifierName);
	}
	
	static synchronized ExtentTest getTest(ITestResult result)
	{
		String testName= result.getMethod().getMethodName();
		
		//Map<String, String> params= result.getTestContext().getCurrentXmlTest().getAllParameters();
		//if(params.size() >0)
		//	testName =testName + "\n" + params.toString();
		
		if(!tests.containsKey(result))
		{
			ExtentTest test = getParentTest(result).createNode(testName);
			tests.put(result, test);
        }
		
		return tests.get(result);
	}
	
	public static void logStep(String description, Status status, Object stepOutcome, HashMap<File, String> stepScreenshots) throws Throwable
	{
		ExtentTest thisTest= getTest(Reporter.getCurrentTestResult());
		
		if(stepScreenshots.size() > 0)
		{
			String scrnshotHtml= buildScreenshotHtml(stepScreenshots);
			description= scrnshotHtml + "</br>" + description;
		}
		
		switch (status) {
		case PASS:
		case SKIP:
		case WARNING:
		case INFO:
			if(stepOutcome.toString().isEmpty() || stepOutcome.toString().equals("{}"))
				thisTest.log(status, description);
			else
				thisTest.log(status, description + "\n" + stepOutcome.toString());
			break;
		case FAIL:
			thisTest.fail(description);
			thisTest.fail((Throwable)stepOutcome);
			break;
		default:
			break;
		}
	}
	
	public static String buildScreenshotHtml(HashMap<File, String> stepScreenshots) throws IOException
	{
		StringBuilder sb= new StringBuilder();
		for (Entry<File, String> scrnshot : stepScreenshots.entrySet()) {
			if(scrnshot.getKey() == null) continue;
			sb
			.append("<a")
			.append(" ")
			.append("target='_blank'").append(" ")
			.append("title='").append(scrnshot.getValue()).append("'").append(" ")
			.append("href='").append(HelperUtil.getScreenshotPath(scrnshot.getKey(), Config.getReportFolder())).append("'").append(" ")
			.append(">")
			.append("<img").append(" ")
			.append("src='").append(HelperUtil.getIconForFile(scrnshot.getKey(), Config.getReportFolder())).append("'").append(" ")
			.append("alt='").append(scrnshot.getKey().getName()).append("'").append(" ")
			.append("height='20' width='30'>")
			.append("</a>");
		}
		
		return sb.toString();
	}

	public static void setTestGroups(Collection<String> values) {
		for (String param : values) {
			if(param.length() < 100) 
				getTest(Reporter.getCurrentTestResult()).assignCategory(param.replace(" ", "_"));
		}
	}
}
