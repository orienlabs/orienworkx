package org.orienlabs.orienworkx.keyword;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestCase {
	
	public String id;
	public String description;
	public List<String> tags;
	
	public List<TestSteps> steps;
	
	public static TestCase build(File testCaseFile) throws Throwable {
		String extn = FilenameUtils.getExtension(testCaseFile.getName()).toLowerCase();
		switch (extn) {
		case "yml":
		case "yaml":
			ObjectMapper testYamlMapper = new ObjectMapper(new YAMLFactory());
			return testYamlMapper.readValue(testCaseFile, TestCase.class);

		default:
			throw new UnknownError("Test file extension " + extn + " is not recongnized");
		}
	}
    
    public String toString()
	{
		return new StringBuilder()
						.append("{")
						.append(id).append(">")
						.append(description).append(">")
						.append(tags.toString())
						.append("}")
						.toString();
	}
}
