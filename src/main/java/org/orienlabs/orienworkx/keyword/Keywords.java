package org.orienlabs.orienworkx.keyword;

import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.lang3.NotImplementedException;
import org.orienlabs.orienworkx.robot.MobileUIDriver;
import org.orienlabs.orienworkx.robot.UIDriver;
import org.orienlabs.orienworkx.robot.UIElement;
import org.orienlabs.orienworkx.utils.HelperUtil;
import org.testng.Assert;

import com.sugarcrm.ws.soap.Int;

import io.appium.java_client.AppiumDriver;

/**
 * @author kumar.anil
 * This class should all keywords as independent methods, with a single argument as {@link HashMap<string, object>}
 */

public class Keywords {
	
	static ScriptEngine jsEngine= new ScriptEngineManager().getEngineByName("JavaScript");
	static HashMap<String, String> keywordAliases= new HashMap<>();
	
	HashMap<String, String> ret= new HashMap<>();
	
	private String getActualText(HashMap<String, Object> args) throws Throwable
	{
		String expected= args.get("expected").toString();
		String actual= null;
		if(args.containsKey("actual"))
			return args.get("actual").toString();
		
		if(args.containsKey("element"))
			return new UIElement(args.get("element").toString()).getText();
		
		ret.put("actual", actual);
		ret.put("expected", expected);
		return actual;
	}
	
	public void compareFiles(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertAlertText(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
		
	public void assertLess(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertLessOrEqual(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertMore(HashMap<String, Object> args) throws Throwable {
		Float expected= HelperUtil.toFloat(args.get("expected").toString());
		Float actual= HelperUtil.toFloat(getActualText(args));
		
		Assert.assertTrue(
				Float.compare(actual, expected) > 0
				, "Actual: " + actual + " found to be <= expected: " + expected );
	}
	
	public void assertMoreOrEqual(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertText(HashMap<String, Object> args) throws Throwable
	{
		String expected= args.get("expected").toString();
		String actual= getActualText(args);
		
		Assert.assertEquals(actual, expected);
	}
	
	public void assertEqual(HashMap<String, Object> args) throws Throwable{

		Float expected= HelperUtil.toFloat(args.get("expected").toString());
		Float actual= HelperUtil.toFloat(getActualText(args));
		
		Assert.assertTrue(
				Float.compare(actual, expected) == 0
				, "Actual: " + actual + " found to be != expected: " + expected );
	}
	
	public void assertNotEqual(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertContains(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertNotContains(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertAttribute(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertStartsWith(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertEndsWith(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertHttpStatus(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertPresent(HashMap<String, Object> args) throws Throwable {
		Assert.assertTrue(new UIElement(args.get("element").toString()).isDisplayed());
	}
	
	public void assertNotPresent(HashMap<String, Object> args) throws Throwable {
		Assert.assertFalse(new UIElement(args.get("element").toString()).isDisplayed());
	}
	public void assertSortAsNumber(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertSortAsString(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertSortAsDate(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertPageUrl(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertPageTitle(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void acceptAlert(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void dismissAlert(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getAlertText(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void setText(HashMap<String, Object> args) throws Throwable{ 
		new UIElement(args.get("element").toString()).clearText();
		new UIElement(args.get("element").toString()).setText(args.get("data").toString());
	}
	
	public void setRandomText(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void setRandomNumber(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void clearText(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void pressKey(HashMap<String, Object> args) throws Throwable{ 
		throw new NotImplementedException("");
	}
	
	public void pressHome(HashMap<String, Object> args) throws Throwable{ 
		MobileUIDriver.goHome();
	}
	
	public void pressEnter(HashMap<String, Object> args) throws Throwable{ 
		MobileUIDriver.pressEnter();
	}
	
	public void submit(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void click(HashMap<String, Object> args) throws Throwable {
		new UIElement(args.get("element").toString()).click();
	}
	
	public void doubleClick(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void clickAndDownload(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void uploadFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void downloadFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void clickAndWait(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void check(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void waitFor(HashMap<String, Object> args) throws Throwable {
		UIElement e= new UIElement(args.get("element").toString());
		UIElement clickOn= null;
		if(args.containsKey("clickOn"))
			clickOn= new UIElement(args.get("clickOn").toString());
		
		int timeout= 10;
		if(args.containsKey("timeout"))
			timeout= Integer.parseInt(args.get("timeout").toString());
		
		if(args.containsKey("intent"))
			e.waitFor(timeout, args.get("intent"));
		else
			e.waitFor(timeout, clickOn);
	}
	
	public void waitForNot(HashMap<String, Object> args) throws Throwable {
		UIElement e = new UIElement(args.get("element").toString());
		UIElement clickOn= null;
		if(args.containsKey("clickOn"))
			clickOn= new UIElement(args.get("clickOn").toString());
		
		int timeout= 10;
		if(args.containsKey("timeout"))
			timeout= Integer.parseInt(args.get("timeout").toString());
		
		e.waitForNot(timeout, clickOn);}
	
	public void dragAndDrop(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void openBrowser(HashMap<String, Object> args) throws Throwable {
		UIDriver.startBrowser();
		if(args.containsKey("url"))
		{
			String requestUrl= args.get("url").toString();
			UIDriver.navigate(requestUrl);
			ret.put("requestedUrl", requestUrl); 
			
			String actualUrl= UIDriver.getUrl();
			if(!actualUrl.equals(requestUrl))
				ret.put("navigatedUrl", actualUrl);
		}
		
		//ret.putAll((Map<String, String>) UIDriver.getActualCapabilities());
	}
	
	public void closeBrowser(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void navigate(HashMap<String, Object> args) throws Throwable{
		String requestUrl= args.get("url").toString();
		UIDriver.navigate(requestUrl);
	}
	
	public void refresh(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void goBack(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getForward(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void closeAllBrowsers(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void switchBrowser(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void runJavaScript(HashMap<String, Object> args) throws Throwable {
		String script= args.get("script").toString();
		String scriptOutcome= "";
		
		if(script.contains("document") || script.contains("arguments"))
		{
			Object out = UIDriver.executeScript(script);
			if (out == null)
				scriptOutcome = "";
			else
				scriptOutcome = out.toString();
		}
		else
			scriptOutcome= jsEngine.eval(script).toString();
		
		if (args.containsKey("store"))
			ret.put(args.get("store").toString(), scriptOutcome);
	}
	
	public void getAttribute(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getText(HashMap<String, Object> args) throws Throwable {
		String text = new UIElement(args.get("element").toString()).getText();

		if (args.containsKey("store"))
			ret.put(args.get("store").toString(), text);
	}
	
	public void getElementCount(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getPageUrl(HashMap<String, Object> args) throws Throwable{
		String pageUrl = UIDriver.getUrl();
		if (args.containsKey("store"))
			ret.put(args.get("store").toString(), pageUrl);
	}
	
	public void readProperties(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void swipeDown(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void swipeUp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void swipeLeft(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void swipeRight(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void selectByIndex(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void selectByValue(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void selectByText(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void runSelectQuery(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void runUpdateQuery(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void runDeleteQuery(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void deleteExcelColumn(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void updateExcelCell(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void updateCsvCell(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readCsvRecord(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readExcelRecord(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readCsvCell(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readExcelCell(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void calculateChecksum(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void copyFileToFolder(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void moveFileToTemp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void copyFileToFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void moveFileToFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void copyFileToTemp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void createTempFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void encryptFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void decryptFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getFileSize(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getFileName(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getFileExtn(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void writeFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void appendToFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void compressFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void renameFile(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void startApp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void installApp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void removeApp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void closeApp(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void runShellCommand(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void sendSoapMsg(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void sendRestRequest(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
			
	public void getRandomNumber(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getRandomString(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getRandomEmail(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void getRandomPhoneNumber(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void sleep(HashMap<String, Object> args) throws Throwable {
		HelperUtil.sleepForSeconds(Integer.parseInt(args.get("timeout").toString()));
	}
	
	public void setVariable(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void toBase64(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void countXmlNodes(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void deleteXmlNodes(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readXmlNode(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void readXmlAttribute(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void updateXmlNode(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void updateXmlAttribute(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void validateXmlSchema(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertXmlNode(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void assertXmlAttribute(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void applyXslt(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	public void formatXml(HashMap<String, Object> args) throws Throwable{ throw new NotImplementedException("");}
	
	static {
		keywordAliases.put("assert>", "assertMore");
		keywordAliases.put("assert=", "assertEqual");
	}
	
	public static String getKeywordFromAlias(String keyword)
	{
		String keywordLowered= keyword.toLowerCase();
		if(keywordAliases.containsKey(keywordLowered))
			return keywordAliases.get(keywordLowered);
		else
			return keyword;
		
	}
	
	public HashMap<String, String> getReturn()
	{
		return ret;
	}
}