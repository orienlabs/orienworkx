package org.orienlabs.orienworkx.keyword;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.logger.Logger;
import org.orienlabs.orienworkx.robot.UIDriver;
import org.orienlabs.orienworkx.utils.HelperUtil;

import com.aventstack.extentreports.Status;

public class KeywordDriver {

	static List<String> screenshotsAfter = Arrays
			.asList(new String[] { "click", "openBrowser"});

	@SuppressWarnings("unchecked")
	public static void runTestCase(File ymlTestCaseFile) throws Throwable {
		TestCase testCase = TestCase.build(ymlTestCaseFile);

		System.out.println(testCase.toString());
		List<Throwable> exceptions = new ArrayList<>();

		for (TestSteps step : testCase.steps) {
			List<String> stepOptions= step.getOptions();
			if(stepOptions.contains("false") || stepOptions.contains("skip"))
				continue;
			
			Status stepStatus = Status.INFO;
			Object stepOutcome = "";
			try {
				if (step.getStepAction().startsWith("assert"))
					stepStatus = Status.PASS;
				
				stepOutcome = step.run();
				if(stepOptions.contains("waitForStable"))
				{
					for (int i = 0; i < 100; i++) {
						HelperUtil.sleepForSeconds(1);
						stepOutcome = step.run();
						if(stepOutcome.toString().equalsIgnoreCase(step.run().toString()))
							break;
					}
				}
				
				Config.setAll((Map<String, String>) stepOutcome);
			}
			catch (Throwable e) {
				if(!stepOptions.contains("optional"))
				{
					if(e.getMessage() != null && (e.getMessage().contains("interactable")
							|| e.getMessage().contains("stale")
							|| e.getMessage().contains("not clickable")
							|| e.getMessage().contains("Error forwarding the new session")
							|| e.getMessage().contains("Element is no longer attached to the DOM")))
					{
						HelperUtil.sleepForSeconds(5);
						
						try {
							stepOutcome = step.run();
							Config.setAll((Map<String, String>) stepOutcome);
							e= null;
						} catch (Exception e2) {
						}
					}
					
					if(e != null)
					{
						stepStatus = Status.FAIL;
						stepOutcome = e;
						exceptions.add(e);
					}
				}
			} finally {
				try {
					if (stepStatus == Status.FAIL || screenshotsAfter.contains(step.getStepAction()) || stepOptions.contains("takeScreenshot"))
					{
						UIDriver.takePngScreenshot(step.getScreenshots());
						UIDriver.getSource(step.getScreenshots());
					}
					
					//if (stepStatus == Status.FAIL)
					//	UIDriver.getSource(step.getScreenshots());
					
				} catch (Exception e2) {
					// TODO: handle exception
				}
				
				if (stepStatus == Status.FAIL && stepOptions.contains("optional")) {
					stepStatus = Status.INFO;
					stepOptions.add("nolog");
				}
				
				if(!stepOptions.contains("nolog"))
					Logger.logStep(step.getStepDescription(), stepStatus, stepOutcome, step.getScreenshots());
			}
			
			if (stepStatus == Status.FAIL && !step.getStepAction().startsWith("assert"))
				break;
		}

		if (exceptions.size() > 0)
			throw exceptions.get(0);
	}
}
