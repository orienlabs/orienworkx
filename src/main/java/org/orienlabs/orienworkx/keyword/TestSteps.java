package org.orienlabs.orienworkx.keyword;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.orienlabs.orienworkx.config.Config;
import org.orienlabs.orienworkx.robot.UIDriver;
import org.orienlabs.orienworkx.robot.UIElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TestSteps {
	
	@JsonProperty("step")
	private String stepDescription;
	
	@JsonProperty("action")
	private String  action;
	
	@JsonProperty("args")
	private HashMap<String, String> args= new HashMap<>();
	
	@JsonProperty("option")
	private String[]  options;
	
	HashMap<File, String> screenshots= new HashMap<>();
	
	public String toString()
	{
		return new StringBuilder()
						.append("{")
						.append(stepDescription).append(">")
						.append(action).append(">")
						.append(args == null ? "" : args.toString()).append(">")
						.append(options)
						.append("}")
						.toString();
	}
	
	public HashMap<String, String> run() throws Throwable {
		if(args == null)
			args= new HashMap<>();
		
		try {
			System.out.println("Running step..." + Config.resolve(this.toString()));
			Method stepMethod = Keywords.class.getMethod(Keywords.getKeywordFromAlias(getStepAction()), HashMap.class);
			Keywords k= new Keywords();
			stepMethod.invoke(k, getArgs());
			
			return k.getReturn();
		} catch (Exception e) {
			throw e.getCause();
		}
	}
	
	public String getStepDescription()
	{
		return Config.resolve(stepDescription);
	}

	public String getStepAction() {
		return Config.resolve(action);
	}
	
	public List<String> getOptions() throws Throwable{
		List<String> returnOptions= new ArrayList<>();
		if(options == null)
			return returnOptions;
			
		for (String option : options) {
			if(option.contains("==")
					|| option.contains("||")
					|| option.contains("&&")
					|| option.contains(">")  || option.contains("<")
					|| option.contains(">=") || option.contains("<=")
					|| option.contains("!=") || option.contains("<>")
					)
				returnOptions.add(UIDriver.executeScript("if(" + Config.resolve(option) + ") true; else false;").toString());
			else
				returnOptions.add(Config.resolve(option));
		}
		
		return returnOptions;
	}
	
	public HashMap<String, String> getArgs() {
		HashMap<String, String> resolvedArgs= new HashMap<>();
		for (Entry<String, String> arg : args.entrySet()) {
			resolvedArgs.put(Config.resolve(arg.getKey()), Config.resolve(arg.getValue()));
		}
		
		return resolvedArgs;
	}
	
	public HashMap<File, String> getScreenshots()
	{
		return screenshots;
	}
}
