package org.orienlabs.orienworkx.providers;

/**
 * @author kumar.anil
 *
 */

public @interface CSVFileParams {
	String path();
	String encoding() default "UTF-8";
}