package org.orienlabs.orienworkx.providers;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.hadoop.hbase.util.Hash;
import org.apache.metamodel.DataContext;
import org.apache.metamodel.DataContextFactory;
import org.apache.metamodel.csv.CsvConfiguration;
import org.apache.metamodel.data.DataSet;
import org.apache.metamodel.data.Row;
import org.apache.metamodel.query.SelectItem;
import org.apache.metamodel.schema.Schema;
import org.apache.metamodel.schema.Table;
import org.testng.annotations.DataProvider;

/**
 * @author kumar.anil
 *
 */

public class TestNGDataProvider {

	@DataProvider(name = "CSVFileLoader")
	public static HashMap<String, String>[] getCsvData(final Method testMethod) {
		CSVFileParams params = testMethod.getAnnotation(CSVFileParams.class);
		if (params != null) {
			String csvPath = params.path();
			String csvEncoding = params.encoding();

			return loadCsvData(new File(csvPath), csvEncoding);
		} else {
			throw new RuntimeException("CSV file details must be provided...");
		}
	}
	
	public static LinkedHashMap<String, String>[] loadCsvData(File csvFile, String encoding) {
		return loadCsvData(csvFile, encoding, CsvConfiguration.DEFAULT_SEPARATOR_CHAR);
	}
	public static LinkedHashMap<String, String>[] loadCsvData(File csvFile, String encoding, char separator) {
		CsvConfiguration conf = new CsvConfiguration(1, encoding, separator, CsvConfiguration.DEFAULT_QUOTE_CHAR, CsvConfiguration.DEFAULT_ESCAPE_CHAR);
		
		DataContext csvContext = DataContextFactory.createCsvDataContext( csvFile, conf );
		Schema schema = csvContext.getDefaultSchema();
		Table[] tables = schema.getTables();
		Table table = tables[0]; // a representation of the csv file name including extension
		DataSet dataSet = csvContext.query()
				.from( table )
				.selectAll()
				.execute();
		List<Row> rows = dataSet.toRows();
		return loadArrayFromApacheModelRows( rows );
	}
	
	private static LinkedHashMap<String, String>[] loadArrayFromApacheModelRows( List<Row> rows ) {
		System.out.println("Total " + rows.size() + " rows in csv data");
		LinkedHashMap<String, String>[] mapList= new LinkedHashMap[rows.size()];
		
		if(rows.size() == 0)
			return mapList;
		
		SelectItem[] cols = rows.get(0).getSelectItems();
		
		int i=0;
		for ( Row r : rows ) {
			LinkedHashMap<String, String> dataRow= new LinkedHashMap<>();
			for (SelectItem col : cols) {
				dataRow.put(col.getColumn().getName(), r.getValue(col).toString());
			}
			mapList[i]= dataRow;
			i++;
		}
		
		return mapList;
	}
}
